//
//  Constants.swift
//  ChildCare
//
//  Created by Mac on 06/02/17.
//  Copyright © 2017 Edward. All rights reserved.
//

import UIKit

struct Constants {
    struct ChildCareAPI {
        static let baseURL = "http://119.81.110.124/~siva/childcare/admin/educatorweb.php"
        static let LOGIN = "http://119.81.110.124/~siva/childcare/admin/educatorweb.php?op=Login&email=%@&password=%@"
        
        static let ACTIVITY = "http://119.81.110.124/~siva/childcare/admin/activityweb.php"
        static let GET_PARENT = "http://119.81.110.124/~siva/childcare/admin/parentweb.php?op=view&educatorid=%@"
        static let GET_CHILD = "http://119.81.110.124/~siva/childcare/admin/childweb.php?id=%@"
        
        static let CHECK_IN = "http://119.81.110.124/~siva/childcare/admin/checkinoutweb.php"
        static let CHECK_OUT = "http://119.81.110.124/~siva/childcare/admin/checkinoutweb.php"
        
//        static let CHECK_OUT_DETAIL = "http://119.81.110.124/~siva/childcare/admin/checkinoutweb.php?op=addCheckout&educator_id=76&parent_id=88&child_id=78&activity_id=82&lan_detail=1.090&lat_detail=3.456"
        
        static let START_DAY = "http://119.81.110.124/~siva/childcare/admin/checkinoutweb.php?op=addStartday&educator_id=%@&lan_detail=%@&lat_detail=%@"
        static let FINISH_DAY = "http://119.81.110.124/~siva/childcare/admin/checkinoutweb.php?op=addStartout&educator_id=%@&lan_detail=%@&lat_detail=%@"
        
        static let CHECKOUT_LIST = "http://119.81.110.124/~siva/childcare/admin/activityweb.php?op=checkoutlist&educatorid=%@"
        
        static let NEWS_LIST = "http://119.81.110.124/~siva/childcare/admin/newsweb.php?op=viewNews"
        
        static let SEND_SUGGESTION = "http://119.81.110.124/~siva/childcare/admin/messageweb.php?op=Sendsuggestions&educatorid=%@&message=%@"
        
        static let MESSAGE_LIST = "http://119.81.110.124/~siva/childcare/admin/messageweb.php?op=viewMessage&educatorid=%@"
        static let SEND_MESSAGE = "http://119.81.110.124/~siva/childcare/admin/messageweb.php?op=SendMessage&educatorid=%@&message=%@"
        static let HANDBOOK = "http://119.81.110.124/~siva/childcare/admin/planweb.php?op=viewHandBook"
        static let TRAINING = "http://119.81.110.124/~siva/childcare/admin/planweb.php?op=viewTrainingBook"
        
        static let PROGRAM = "http://119.81.110.124/~siva/childcare/admin/planweb.php?op=viewProgramming"
        
        static let RESOURCES = "http://childcareforyou.com.au/resources/"
        static let CONTACT = "http://childcareforyou.com.au/appcontact"
        static let RISK_FORM = "http://119.81.110.124/~siva/childcare/admin/riskweb.php"
        
        static let EXTRA_LIST = "http://119.81.110.124/~siva/childcare/admin/newsweb.php?op=viewExtras"
        static let ADD_ORDER_EXTRAS = "http://119.81.110.124/~siva/childcare/admin/newsweb.php?op=addExtras&educatorid=%@&extra_id=%@&extra_qty=%@&extra_type=%@"
        static let CHILDREN_LIST = "http://119.81.110.124/~siva/childcare/admin/childweb.php?op=viewAll&educator_id=%@"
        
    }
    static let DAY_STARTED = "day_started"
    static let ONE_SIGNAL_API_KEY = "693967a6-a008-46f8-96cc-ff0ffbbb653c"
    
}
