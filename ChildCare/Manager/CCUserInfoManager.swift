//
//  CCUserInfoManager.swift
//  ChildCare
//
//  Created by Mac on 06/02/17.
//  Copyright © 2017 Edward. All rights reserved.
//

import UIKit

class CCUserInfoManager {
    
    static let sharedInstance = CCUserInfoManager()
    
    func needLogin() -> Bool {
        return self.getCurrentUser() == nil
    }
    
    func saveCurrentUser(user: CCUser) {
        let data = NSKeyedArchiver.archivedData(withRootObject: user)
        UserDefaults.standard.set(data, forKey: "currentUser")
    }
    
    func getCurrentUser() -> CCUser? {
        var user: CCUser
        if let data = UserDefaults.standard.object(forKey: "currentUser") as? Data {
            user = NSKeyedUnarchiver.unarchiveObject(with: data) as! CCUser
            return user
        }
        return nil
    }
    
    func removeCurrentUser() {
        UserDefaults.standard.removeObject(forKey: "currentUser")
        UserDefaults.standard.synchronize()
    }

}
