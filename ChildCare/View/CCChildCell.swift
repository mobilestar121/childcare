//
//  CCChildCell.swift
//  ChildCare
//
//  Created by Mac on 07/02/17.
//  Copyright © 2017 Edward. All rights reserved.
//

import UIKit
import BEMCheckBox

protocol CCChildCellDelegate {
    func checkBoxClicked(_ checkBox: BEMCheckBox)
}

class CCChildCell: UITableViewCell, BEMCheckBoxDelegate {
    
    @IBOutlet weak var lblChildName: UILabel!
    @IBOutlet weak var checkBox: BEMCheckBox!
    
    var deleage: CCChildCellDelegate?

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        self.checkBox.animationDuration = 0.1
        self.checkBox.onAnimationType = .fill
    }
    func didTap(_ checkBox: BEMCheckBox) {
        self.deleage?.checkBoxClicked(checkBox)
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
