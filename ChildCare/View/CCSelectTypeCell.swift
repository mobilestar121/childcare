//
//  CCSelectTypeCell.swift
//  ChildCare
//
//  Created by Mac on 07/02/17.
//  Copyright © 2017 Edward. All rights reserved.
//

import UIKit

class CCSelectTypeCell: UITableViewCell {

    @IBOutlet weak var lblSelect: UILabel!
    @IBOutlet weak var imgDownArrow: UIImageView!
    @IBOutlet weak var loadingIndicator: UIActivityIndicatorView!
    
    @IBOutlet weak var containerView: UIView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        self.containerView.layer.borderWidth = 1.0
        self.containerView.layer.borderColor = UIColor(netHex: 0xEEEEEE).cgColor
        self.containerView.layer.cornerRadius = 2.0
        self.containerView.layer.masksToBounds = true
    }
    
    func showIndicator(isHidden: Bool) {
        self.imgDownArrow.isHidden = !isHidden
        self.loadingIndicator.isHidden = isHidden
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
