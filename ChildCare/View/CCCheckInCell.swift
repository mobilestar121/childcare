//
//  CCCheckInCell.swift
//  ChildCare
//
//  Created by Mac on 07/02/17.
//  Copyright © 2017 Edward. All rights reserved.
//

import UIKit

class CCCheckInCell: UITableViewCell {
    @IBOutlet weak var lblCheckIn: UILabel!

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        self.layoutIfNeeded()
        self.setNeedsLayout()
        self.lblCheckIn.layer.cornerRadius = self.lblCheckIn.frame.height/2
        self.lblCheckIn.layer.masksToBounds = true
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
