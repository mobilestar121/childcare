//
//  CCProfileHeaderCell.swift
//  ChildCare
//
//  Created by Mac on 10/02/17.
//  Copyright © 2017 Edward. All rights reserved.
//

import UIKit
import AlamofireImage

class CCProfileHeaderCell: UITableViewCell {
    
    @IBOutlet weak var lblName: UILabel!
    @IBOutlet weak var imgProfile: UIImageView!

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        self.contentView.layoutIfNeeded()
        self.contentView.setNeedsLayout()
        self.imgProfile.layer.cornerRadius = self.imgProfile.frame.width/2
        self.imgProfile.layer.masksToBounds = true
    }
    
    func configureCell(with URLString: String, placeholderImage: UIImage) {
        let size = imgProfile.frame.size
        
        imgProfile.af_setImage(
            withURL: URL(string: URLString)!,
            placeholderImage: placeholderImage,
            filter: AspectScaledToFillSizeWithRoundedCornersFilter(size: size, radius: size.width/2),
            imageTransition: .crossDissolve(0.2)
        )
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
