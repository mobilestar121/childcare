//
//  CCNewsCell.swift
//  ChildCare
//
//  Created by Mac on 15/02/17.
//  Copyright © 2017 Edward. All rights reserved.
//

import UIKit
import AlamofireImage

class CCNewsCell: UITableViewCell {
    
    @IBOutlet weak var newsImageView: UIImageView!
    @IBOutlet weak var lblDate: UILabel!
    @IBOutlet weak var lblDescription: UILabel!

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    func configureCell(with URLString: String, placeholderImage: UIImage) {
        let size = newsImageView.frame.size
        
        newsImageView.af_setImage(
            withURL: URL(string: URLString)!,
            placeholderImage: placeholderImage,
            filter: AspectScaledToFillSizeWithRoundedCornersFilter(size: size, radius: 0.0),
            imageTransition: .crossDissolve(0.2)
        )
    }
    
    override func prepareForReuse() {
        super.prepareForReuse()
        
        newsImageView.af_cancelImageRequest()
        newsImageView.layer.removeAllAnimations()
        newsImageView.image = nil
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
