//
//  CCMenuCell.swift
//  ChildCare
//
//  Created by Mac on 09/02/17.
//  Copyright © 2017 Edward. All rights reserved.
//

import UIKit

class CCMenuCell: UITableViewCell {
    
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var imgIcon: UIImageView!

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
