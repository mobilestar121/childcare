//
//  CCFormCell.swift
//  ChildCare
//
//  Created by Mac on 08/02/17.
//  Copyright © 2017 Edward. All rights reserved.
//

import UIKit

class CCFormCell: UITableViewCell {
    
    @IBOutlet weak var lblFormName: UILabel!

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
