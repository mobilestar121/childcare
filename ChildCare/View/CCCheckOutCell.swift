//
//  CCCheckOutCell.swift
//  ChildCare
//
//  Created by Mac on 15/02/17.
//  Copyright © 2017 Edward. All rights reserved.
//

import UIKit

class CCCheckOutCell: UITableViewCell {
    
    @IBOutlet weak var lblActivityName: UILabel!
    @IBOutlet weak var lblChildrenName: UILabel!
    @IBOutlet weak var btnCheckout: UIButton!

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
