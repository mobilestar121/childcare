//
//  ChildCareAPI.swift
//  ChildCare
//
//  Created by Mac on 06/02/17.
//  Copyright © 2017 Edward. All rights reserved.
//

import UIKit
import Alamofire

class ChildCareService {
    static let sharedInstance = ChildCareService()
    var sessionManager = Alamofire.SessionManager()
    /*
    init() {
        let headers: HTTPHeaders = [
            "Content-Type": "application/x-www-form-urlencoded"
        ]
        
        let configuration = URLSessionConfiguration.default
        configuration.httpAdditionalHeaders = headers
        
        self.sessionManager = Alamofire.SessionManager(configuration: configuration)
    } */
    
    func login(emailStr: String, passStr: String, onSuccess:@escaping (_ result: DataResponse<Any>) -> Void, onFailure:@escaping (_ error: Error) -> Void) {
        let url = String(format: Constants.ChildCareAPI.LOGIN, emailStr, passStr)
        self.sessionManager.request(url).responseJSON { response in
            switch response.result {
            case .success:
                onSuccess(response)
            case .failure(let error):
                onFailure(error)
            }
        }
    }
    
    func getActivity(onSuccess:@escaping (_ result: DataResponse<Any>) -> Void, onFailure:@escaping (_ error: Error) -> Void) {
        let url = String(format: Constants.ChildCareAPI.ACTIVITY)
        self.sessionManager.request(url).responseJSON { response in
            switch response.result {
            case .success:
                onSuccess(response)
            case .failure(let error):
                onFailure(error)
            }
        }
    }
    
    func getParent(educatorId:String, onSuccess:@escaping (_ result: DataResponse<Any>) -> Void, onFailure:@escaping (_ error: Error) -> Void) {
        let url = String(format: Constants.ChildCareAPI.GET_PARENT, educatorId)
        self.sessionManager.request(url).responseJSON { response in
            switch response.result {
            case .success:
                onSuccess(response)
            case .failure(let error):
                onFailure(error)
            }
        }
    }
    
    func getChild(childId:String, onSuccess:@escaping (_ result: DataResponse<Any>) -> Void, onFailure:@escaping (_ error: Error) -> Void) {
        let url = String(format: Constants.ChildCareAPI.GET_CHILD, childId)
        self.sessionManager.request(url).responseJSON { response in
            switch response.result {
            case .success:
                onSuccess(response)
            case .failure(let error):
                onFailure(error)
            }
        }
    }
    
    func getExtraChildren(educatorId:String, onSuccess:@escaping (_ result: DataResponse<Any>) -> Void, onFailure:@escaping (_ error: Error) -> Void) {
        let url = String(format: Constants.ChildCareAPI.CHILDREN_LIST, educatorId)
        self.sessionManager.request(url).responseJSON { response in
            switch response.result {
            case .success:
                onSuccess(response)
            case .failure(let error):
                onFailure(error)
            }
        }
    }
    
    func getExtraList(onSuccess:@escaping (_ result: DataResponse<Any>) -> Void, onFailure:@escaping (_ error: Error) -> Void) {
        let url = String(format: Constants.ChildCareAPI.EXTRA_LIST)
        self.sessionManager.request(url).responseJSON { response in
            switch response.result {
            case .success:
                onSuccess(response)
            case .failure(let error):
                onFailure(error)
            }
        }
    }
    
    func addOrderExtra(educatorId:String, extraId:String, qtyId:String, type:String, onSuccess:@escaping (_ result: DataResponse<Any>) -> Void, onFailure:@escaping (_ error: Error) -> Void) {
        let url = String(format: Constants.ChildCareAPI.ADD_ORDER_EXTRAS, educatorId, extraId, qtyId, type)
        self.sessionManager.request(url).responseJSON { response in
            switch response.result {
            case .success:
                onSuccess(response)
            case .failure(let error):
                onFailure(error)
            }
        }
    }
    
    func checkIn(educatorId:String, parentId:String, childId:String, activityId:String, lat:String, lon:String, signature:String, onSuccess:@escaping (_ result: DataResponse<Any>) -> Void, onFailure:@escaping (_ error: Error) -> Void) -> Void {
        let parameters: Parameters = [
            "op": "addCheckin",
            "educator_id":educatorId,
            "parent_id":parentId,
            "child_id":childId,
            "lan_detail":lat,
            "lat_detail":lon,
            "signature":signature
        ]
        self.sessionManager.request(Constants.ChildCareAPI.CHECK_IN, method: .post, parameters: parameters, encoding: URLEncoding.default).responseJSON { response in
            switch response.result {
            case .success:
                onSuccess(response)
            case .failure(let error):
                onFailure(error)
            }
        }
    }
    
    func checkOutDetail(educatorId:String, activityId:String, lat:String, lon:String, signature:String, onSuccess:@escaping (_ result: DataResponse<Any>) -> Void, onFailure:@escaping (_ error: Error) -> Void) -> Void {
        let parameters: Parameters = [
            "op": "addCheckout",
            "educator_id":educatorId,
            "activity_id":activityId,
            "lan_detail":lat,
            "lat_detail":lon,
            "signature":signature
        ]
        self.sessionManager.request(Constants.ChildCareAPI.CHECK_OUT, method: .post, parameters: parameters, encoding: URLEncoding.default).responseJSON { response in
            switch response.result {
            case .success:
                onSuccess(response)
            case .failure(let error):
                onFailure(error)
            }
        }
    }
    
    func startDay(educatorId:String, lat:String, lon:String, onSuccess:@escaping (_ result: DataResponse<Any>) -> Void, onFailure:@escaping (_ error: Error) -> Void) -> Void {
        let url = String(format: Constants.ChildCareAPI.START_DAY, educatorId, lat, lon)
        self.sessionManager.request(url).responseJSON { response in
            switch response.result {
            case .success:
                onSuccess(response)
            case .failure(let error):
                onFailure(error)
            }
        }
    }
    
    func finishDay(educatorId:String, lat:String, lon:String, onSuccess:@escaping (_ result: DataResponse<Any>) -> Void, onFailure:@escaping (_ error: Error) -> Void) -> Void {
        let url = String(format: Constants.ChildCareAPI.FINISH_DAY, educatorId, lat, lon)
        self.sessionManager.request(url).responseJSON { response in
            switch response.result {
            case .success:
                onSuccess(response)
            case .failure(let error):
                onFailure(error)
            }
        }
    }
    func getCheckoutList(educatorId:String, onSuccess:@escaping (_ result: DataResponse<Any>) -> Void, onFailure:@escaping (_ error: Error) -> Void) -> Void {
        let url = String(format: Constants.ChildCareAPI.CHECKOUT_LIST, educatorId)
        self.sessionManager.request(url).responseJSON { response in
            switch response.result {
            case .success:
                onSuccess(response)
            case .failure(let error):
                onFailure(error)
            }
        }
    }
    
    func getNews(onSuccess:@escaping (_ result: DataResponse<Any>) -> Void, onFailure:@escaping (_ error: Error) -> Void) -> Void {
        let url = String(format: Constants.ChildCareAPI.NEWS_LIST)
        self.sessionManager.request(url).responseJSON { response in
            switch response.result {
            case .success:
                onSuccess(response)
            case .failure(let error):
                onFailure(error)
            }
        }
    }
    func submitSuggestion(educatorId:String, message: String, onSuccess:@escaping (_ result: DataResponse<Any>) -> Void, onFailure:@escaping (_ error: Error) -> Void) -> Void {
        let url = String(format: Constants.ChildCareAPI.SEND_SUGGESTION, educatorId, message)
        self.sessionManager.request(url).responseJSON { response in
            switch response.result {
            case .success:
                onSuccess(response)
            case .failure(let error):
                onFailure(error)
            }
        }
    }
    
    func getMessageList(educatorId:String, onSuccess:@escaping (_ result: DataResponse<Any>) -> Void, onFailure:@escaping (_ error: Error) -> Void) -> Void {
        let url = String(format: Constants.ChildCareAPI.MESSAGE_LIST, educatorId)
        self.sessionManager.request(url).responseJSON { response in
            switch response.result {
            case .success:
                onSuccess(response)
            case .failure(let error):
                onFailure(error)
            }
        }
    }
    
    func sendMessage(educatorId:String, message:String, onSuccess:@escaping (_ result: DataResponse<Any>) -> Void, onFailure:@escaping (_ error: Error) -> Void) -> Void {
        let url = String(format: Constants.ChildCareAPI.SEND_MESSAGE, educatorId, message)
        self.sessionManager.request(url).responseJSON { response in
            switch response.result {
            case .success:
                onSuccess(response)
            case .failure(let error):
                onFailure(error)
            }
        }
    }
    
    func getHandbook(onSuccess:@escaping (_ result: DataResponse<Any>) -> Void, onFailure:@escaping (_ error: Error) -> Void) -> Void {
        let url = String(format: Constants.ChildCareAPI.HANDBOOK)
        self.sessionManager.request(url).responseJSON { response in
            switch response.result {
            case .success:
                onSuccess(response)
            case .failure(let error):
                onFailure(error)
            }
        }
    }
    func getTraining(onSuccess:@escaping (_ result: DataResponse<Any>) -> Void, onFailure:@escaping (_ error: Error) -> Void) -> Void {
        let url = String(format: Constants.ChildCareAPI.TRAINING)
        self.sessionManager.request(url).responseJSON { response in
            switch response.result {
            case .success:
                onSuccess(response)
            case .failure(let error):
                onFailure(error)
            }
        }
    }
    func getProgramm(onSuccess:@escaping (_ result: DataResponse<Any>) -> Void, onFailure:@escaping (_ error: Error) -> Void) -> Void {
        let url = String(format: Constants.ChildCareAPI.PROGRAM)
        self.sessionManager.request(url).responseJSON { response in
            switch response.result {
            case .success:
                onSuccess(response)
            case .failure(let error):
                onFailure(error)
            }
        }
    }
    
    
    /////////////// POST method
    
    func postRiskForm(educator_id:String,
                      excurstion_destination: String,
                      date_excursion: String,
                      departure_arrive_times: String,
                      water_hazards: String,
                      risk_assessment: String,
                      method_transport: String,
                      excursion_coordinator: String,
                      contactnumber_BH: String,
                      contactnumber_M: String,
                      number_children: String,
                      number_educators: String,
                      educator_child_ratio: String,
                      excursion_checklist: String,
                      other_detail: String,
                      created_location: String,
                      onSuccess:@escaping (_ result: DataResponse<Any>) -> Void, onFailure:@escaping (_ error: Error) -> Void) {
        let parameters: Parameters = [
            "op": "riskInsert",
            "educator_id":educator_id,
            "excurstion_destination":excurstion_destination,
            "date_excursion": date_excursion,
            "departure_arrive_times":departure_arrive_times,
            "water_hazards": water_hazards,
            "risk_assessment":risk_assessment,
            "method_transport": method_transport,
            "excursion_coordinator":excursion_coordinator,
            "contactnumber_BH": contactnumber_BH,
            "contactnumber_M":contactnumber_M,
            "number_children": number_children,
            "number_educators":number_educators,
            "educator_child_ratio": educator_child_ratio,
            "excursion_checklist":excursion_checklist,
            "other_detail": other_detail,
            "created_location":created_location
        ]
        self.sessionManager.request(Constants.ChildCareAPI.RISK_FORM, method: .post, parameters: parameters, encoding: URLEncoding.default).responseJSON { response in
            switch response.result {
            case .success:
                onSuccess(response)
            case .failure(let error):
                onFailure(error)
            }
        }
    }    
    
    func postRegularForm(educator_id:String,
                      educators_full_name: String,
                      destination_outing: String,
                      name_child: String,
                      reason_excursion: String,
                      time_leave: String,
                      time_arrive: String,
                      number_children_attending_outing: String,
                      ratio_educators_children: String,
                      method_transport: String,
                      other_detail: String,
                      other_staff_adult_accomanying_supervising: String,
                      i_permission: String,
                      permission_child_children: String,
                      particpate_outing_educator: String,
                      phone_no: String,
                      mobile: String,
                      parent_gurdian_name: String,
                      siganture: String,
                      created_location: String,
                      onSuccess:@escaping (_ result: DataResponse<Any>) -> Void, onFailure:@escaping (_ error: Error) -> Void) {
        let parameters: Parameters = [
            "op": "regularinsert",
            "educator_id":educator_id,
            "educators_full_name":educators_full_name,
            "destination_outing": destination_outing,
            "name_child":name_child,
            "reason_excursion": reason_excursion,
            "time_leave":time_leave,
            "time_arrive": time_arrive,
            "number_children_attending_outing":number_children_attending_outing,
            "ratio_educators_children": ratio_educators_children,
            "method_transport":method_transport,
            "other_detail": other_detail,
            "other_staff_adult_accomanying_supervising":other_staff_adult_accomanying_supervising,
            "i_permission":i_permission,
            "permission_child_children": permission_child_children,
            "particpate_outing_educator":particpate_outing_educator,
            "phone_no": phone_no,
            "mobile": mobile,
            "parent_gurdian_name": parent_gurdian_name,
            "siganture": siganture,
            "created_location":created_location
        ]
        self.sessionManager.request(Constants.ChildCareAPI.RISK_FORM, method: .post, parameters: parameters, encoding: URLEncoding.default).responseJSON { response in
            switch response.result {
            case .success:
                onSuccess(response)
            case .failure(let error):
                onFailure(error)
            }
        }
    }
    
    func postExcusionForm(educator_id:String,
                         educators_full_name: String,
                         dats_excursion: String,
                         name_child: String,
                         reason_excursion: String,
                         time_leave: String,
                         time_arrive: String,
                         number_children_attending_outing: String,
                         ratio_educators_children: String,
                         method_transport: String,
                         other_detail: String,
                         other_staff_adult_accomanying_supervising: String,
                         i_permission: String,
                         permission_child_children: String,
                         particpate_outing_educator: String,
                         phone_no: String,
                         mobile: String,
                         parent_gurdian_name: String,
                         siganture: String,
                         created_location: String,
                         onSuccess:@escaping (_ result: DataResponse<Any>) -> Void, onFailure:@escaping (_ error: Error) -> Void) {
        let parameters: Parameters = [
            "op": "excusioninsert",
            "educator_id":educator_id,
            "educators_full_name":educators_full_name,
            "dats_excursion": dats_excursion,
            "name_child":name_child,
            "reason_excursion": reason_excursion,
            "time_leave":time_leave,
            "time_arrive": time_arrive,
            "number_children_attending_outing":number_children_attending_outing,
            "ratio_educators_children": ratio_educators_children,
            "method_transport":method_transport,
            "other_detail": other_detail,
            "other_staff_adult_accomanying_supervising":other_staff_adult_accomanying_supervising,
            "i_permission":i_permission,
            "permission_child_children": permission_child_children,
            "particpate_outing_educator":particpate_outing_educator,
            "phone_no": phone_no,
            "mobile": mobile,
            "parent_gurdian_name": parent_gurdian_name,
            "siganture": siganture,
            "created_location":created_location
        ]
        self.sessionManager.request(Constants.ChildCareAPI.RISK_FORM, method: .post, parameters: parameters, encoding: URLEncoding.default).responseJSON { response in
            switch response.result {
            case .success:
                onSuccess(response)
            case .failure(let error):
                onFailure(error)
            }
        }
    }
}
