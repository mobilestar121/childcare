
//
//  CCNews.swift
//  ChildCare
//
//  Created by Mac on 15/02/17.
//  Copyright © 2017 Edward. All rights reserved.
//

import UIKit

class CCNews {
    var id: String?
    var news_name: String?
    var news_desc: String?
    var news_type: String?
    var news_image: String?
    var status: String?
    var date_on: String?
}
