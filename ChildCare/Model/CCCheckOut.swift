//
//  CCCheckOut.swift
//  ChildCare
//
//  Created by Mac on 15/02/17.
//  Copyright © 2017 Edward. All rights reserved.
//

import UIKit

class CCCheckOut {
    var id: String?
    var activity_name: String?
    var child_details: String?
    var parent_details: String?
    var check_out: Bool?
    var activity_checkin: Bool?
    var activity_checkout: Bool?

}
