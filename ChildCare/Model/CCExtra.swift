//
//  CCExtra.swift
//  ChildCare
//
//  Created by Mac on 30/03/17.
//  Copyright © 2017 Edward. All rights reserved.
//

import UIKit

class CCExtra {
    var id: String?
    var extras_name: String?
    var extras_price: String?
    var extras_cat: String?
    var status: String?
    var date_on: String?
}
