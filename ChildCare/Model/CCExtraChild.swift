//
//  CCExtraChild.swift
//  ChildCare
//
//  Created by Mac on 30/03/17.
//  Copyright © 2017 Edward. All rights reserved.
//

import UIKit

class CCExtraChild {
    var id : String?
    var children_name: String?
    var parent_id: String?
    var children_age: String?
    var children_gender: String?
    var status: String?
    var date_on: String?
}
