//
//  CCMessage.swift
//  ChildCare
//
//  Created by Mac on 17/02/17.
//  Copyright © 2017 Edward. All rights reserved.
//

import UIKit

class CCMessage {
    var sender_id: String?
    var message: String?
    var message_date: String?
}
