//
//  CCUser.swift
//  ChildCare
//
//  Created by Mac on 06/02/17.
//  Copyright © 2017 Edward. All rights reserved.
//

import UIKit

class CCUser: NSObject, NSCoding {
    var id: String = ""
    var coach_name: String = ""
    var coach_lname: String = ""
    var phone: String = ""
    var mobile: String = ""
    var email: String = ""
    var profile_pic: String = ""
    
    override init() {
        
    }
    
    required init?(coder aDecoder: NSCoder) {
        // super.init(coder:) is optional, see notes below
        self.id = aDecoder.decodeObject(forKey: "id") as! String
        self.coach_name = aDecoder.decodeObject(forKey: "coach_name") as! String
        self.coach_lname = aDecoder.decodeObject(forKey: "coach_lname") as! String
        self.phone = aDecoder.decodeObject(forKey: "phone") as! String
        self.mobile = aDecoder.decodeObject(forKey: "mobile") as! String
        self.email = aDecoder.decodeObject(forKey: "email") as! String
        self.profile_pic = aDecoder.decodeObject(forKey: "profile_pic") as! String
    }
    
    func encode(with aCoder: NSCoder) {
        aCoder.encode(self.id, forKey: "id")
        aCoder.encode(self.coach_name, forKey: "coach_name")
        aCoder.encode(self.coach_lname, forKey: "coach_lname")
        aCoder.encode(self.phone, forKey: "phone")
        aCoder.encode(self.mobile, forKey: "mobile")
        aCoder.encode(self.email, forKey: "email")
        aCoder.encode(self.profile_pic, forKey: "profile_pic")
    }
}
