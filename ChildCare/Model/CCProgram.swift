//
//  CCProgram.swift
//  ChildCare
//
//  Created by Mac on 21/02/17.
//  Copyright © 2017 Edward. All rights reserved.
//

import UIKit

class CCProgram {
    var id: String?
    var program_name: String?
    var program_file: String?
    var status: String?
    var date_on: String?
}
