//
//  CCParent.swift
//  ChildCare
//
//  Created by Mac on 07/02/17.
//  Copyright © 2017 Edward. All rights reserved.
//

import UIKit

class CCParent {    
    var id: String?
    var parent_name: String?
    var parent_lname: String?
    var mobile: String?
    var email: String?
    var password_c: String?
    var status: String?
    var parent_agreement: String?
    var children: String?
    var date_on: String?
}
