//
//  CCActivity.swift
//  ChildCare
//
//  Created by Mac on 07/02/17.
//  Copyright © 2017 Edward. All rights reserved.
//

import UIKit

class CCActivity {
    var id: String?
    var activity_name: String?
    var activity_desc: String?
    var activity_checkin: Bool?
    var activity_checkout: Bool?
}
