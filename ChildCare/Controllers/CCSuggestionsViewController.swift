//
//  CCSuggestionsViewController.swift
//  ChildCare
//
//  Created by Mac on 06/02/17.
//  Copyright © 2017 Edward. All rights reserved.
//

import UIKit
import SVProgressHUD

class CCSuggestionsViewController: CCContentViewController {
    
    @IBOutlet weak var textView: UITextView!
    @IBOutlet weak var btnSubmit: UIButton!

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        navigationItem.title = "Suggestion"
        
        addRightNavBar()
        
        btnSubmit.layer.cornerRadius = 24.0
        btnSubmit.layer.masksToBounds = true
        self.textView.layer.borderWidth = 1.0
        self.textView.layer.borderColor = UIColor(netHex: 0xEEEEEE).cgColor
    }
    
    func addRightNavBar() {
        let rightButton = UIButton()
        rightButton.setImage(UIImage(named: "contact-icon"), for: .normal)
        rightButton.frame = CGRect(x:0, y:0, width:72, height:30)
        rightButton.imageEdgeInsets = UIEdgeInsetsMake(3, 0, 0, -10)
        rightButton.addTarget(self, action: #selector(openContact), for: .touchUpInside)
        self.navigationItem.setRightBarButton(UIBarButtonItem(customView: rightButton), animated: true);
    }
    func openContact() {
        let mobileNumber = CCUserInfoManager.sharedInstance.getCurrentUser()?.mobile
        self.callNumber(phoneNumber: mobileNumber!)
    }
    private func callNumber(phoneNumber:String) {
        if let phoneCallURL = URL(string: "tel://\(phoneNumber)") {
            let application:UIApplication = UIApplication.shared
            if (application.canOpenURL(phoneCallURL)) {
                application.open(phoneCallURL, options: [:], completionHandler: nil)
            }
        }
    }
    
    @IBAction func submitAction(_ sender: AnyObject) {
        if (self.textView.text == "") {
            self.showAlert(title: "Error!", message: "Please fill your suggestion")
            return
        }
        let educatorId = CCUserInfoManager.sharedInstance.getCurrentUser()?.id
        var message = self.textView.text.replacingOccurrences(of: " ", with: "%20")
        message = message.replacingOccurrences(of: "\n", with: "%0D%0A")
        
        SVProgressHUD.show()
        ChildCareService.sharedInstance.submitSuggestion(educatorId: educatorId!, message: message, onSuccess: { (response) in
            SVProgressHUD.dismiss()
            if let result = response.result.value as? NSDictionary{
                if let status = result["status"] as? Bool {
                    if status == true {
                        self.showAlert(title: "Success!", message: "Your suggestion submitted successfully")
                    } else {
                        if let err = result["error"] as? NSDictionary {
                            if let message = err["message"] as? String {
                                self.showAlert(title:"Error!", message: message)
                                return
                            }
                        }
                    }
                }
            }
            }, onFailure: { (error) in
                SVProgressHUD.dismiss()
                self.showAlert(title:"Error!", message: error.localizedDescription)
        })
    }
    
    func showAlert(title:String, message:String) {
        let alert = UIAlertController(title: title, message: message, preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "Ok", style: .default, handler: nil))
        self.present(alert, animated: true, completion: nil)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
