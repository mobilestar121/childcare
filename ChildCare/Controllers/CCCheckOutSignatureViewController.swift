//
//  CCCheckOutSignatureViewController.swift
//  ChildCare
//
//  Created by Mac on 01/03/17.
//  Copyright © 2017 Edward. All rights reserved.
//

import UIKit

class CCCheckOutSignatureViewController: UIViewController {
    
    @IBOutlet weak var signatureView: SignatureView!
    @IBOutlet weak var btnCheckOut: UIButton!
    
    var paramDict:[String:String] = [:]
    var signBaseString: String?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
        self.navigationItem.title = "Parent Signature"
        addRightNavBar()
        
        btnCheckOut.layer.cornerRadius = 24.0
        btnCheckOut.layer.masksToBounds = true
        self.signatureView.layer.borderWidth = 1.0
        self.signatureView.layer.borderColor = UIColor(netHex: 0xEEEEEE).cgColor
    }
    
    func addRightNavBar() {
        let letfButton = UIButton()
        letfButton.setImage(UIImage(named: "back_icon"), for: .normal)
        letfButton.frame = CGRect(x:-40, y:0, width:30, height:30)
        letfButton.imageEdgeInsets = UIEdgeInsetsMake(0, -20, 0, 0)
        letfButton.addTarget(self, action: #selector(back), for: .touchUpInside)
        self.navigationItem.setLeftBarButton(UIBarButtonItem(customView: letfButton), animated: true)
        
        let rightButton = UIButton()
        rightButton.setImage(UIImage(named: "contact-icon"), for: .normal)
        rightButton.frame = CGRect(x:0, y:0, width:72, height:30)
        rightButton.imageEdgeInsets = UIEdgeInsetsMake(3, 0, 0, -10)
        rightButton.addTarget(self, action: #selector(openContact), for: .touchUpInside)
        self.navigationItem.setRightBarButton(UIBarButtonItem(customView: rightButton), animated: true)
    }
    func openContact() {
        let mobileNumber = CCUserInfoManager.sharedInstance.getCurrentUser()?.mobile
        self.callNumber(phoneNumber: mobileNumber!)
    }
    private func callNumber(phoneNumber:String) {
        if let phoneCallURL = URL(string: "tel://\(phoneNumber)") {
            let application:UIApplication = UIApplication.shared
            if (application.canOpenURL(phoneCallURL)) {
                application.open(phoneCallURL, options: [:], completionHandler: nil)
            }
        }
    }
    func back() {
        _ = self.navigationController?.popViewController(animated: true)
    }
    
    func showAlert(title:String, message:String) {
        let alert = UIAlertController(title: title, message: message, preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "Ok", style: .default, handler: nil))
        self.present(alert, animated: true, completion: nil)
    }
    
    @IBAction func onCheckOutAction(_ sender: AnyObject) {
        let educatorId = paramDict["educatorId"]
        let activityId = paramDict["activityId"]
        let lat = paramDict["lat"]
        let lon = paramDict["lon"]
        
        let signatureImage = signatureView.getSignature()
        let signData = UIImageJPEGRepresentation(signatureImage, 0.1)
        signBaseString = signData?.base64EncodedString(options: .endLineWithCarriageReturn)
        
        ChildCareService.sharedInstance.checkOutDetail(educatorId: educatorId!, activityId: activityId!, lat: lat!, lon: lon!, signature: signBaseString!, onSuccess: { (response) in
            if let result = response.result.value as? NSDictionary{
                if let status = result["status"] as? Bool {
                    if status == true {
                        let alert = UIAlertController(title: "ChildCare", message: "Checked out succesfully", preferredStyle: .alert)
                        alert.addAction(UIAlertAction(title: "Ok", style: .default) {action in
                            _ = self.navigationController?.popToRootViewController(animated: true)
                        })
                        self.present(alert, animated: true, completion: nil)
                    } else {
                        if let err = result["error"] as? NSDictionary {
                            if let message = err["message"] as? String {
                                self.showAlert(title:"Error!", message: message)
                                return
                            }
                        }
                    }
                }
            }
            }, onFailure: { (error) in
                self.showAlert(title:"Error!", message: error.localizedDescription)
        })
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    

}
