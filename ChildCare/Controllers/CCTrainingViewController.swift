//
//  CCTrainingViewController.swift
//  ChildCare
//
//  Created by Mac on 14/03/17.
//  Copyright © 2017 Edward. All rights reserved.
//

import UIKit
import SVProgressHUD

class CCTrainingViewController: UIViewController {

    @IBOutlet weak var webView: UIWebView!
    var handbooksArray = [CCHandbook]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
        navigationItem.title = "Training"
        
        addLeftNavBar()
        
        requestHandBook()
        
    }
    
    func addLeftNavBar() {
        let rightButton = UIButton()
        rightButton.setImage(UIImage(named: "contact-icon"), for: .normal)
        rightButton.frame = CGRect(x:0, y:0, width:72, height:30)
        rightButton.imageEdgeInsets = UIEdgeInsetsMake(3, 0, 0, -10)
        rightButton.addTarget(self, action: #selector(openContact), for: .touchUpInside)
        self.navigationItem.setRightBarButton(UIBarButtonItem(customView: rightButton), animated: true);
    }
    func openContact() {
        let mobileNumber = CCUserInfoManager.sharedInstance.getCurrentUser()?.mobile
        self.callNumber(phoneNumber: mobileNumber!)
    }
    private func callNumber(phoneNumber:String) {
        if let phoneCallURL = URL(string: "tel://\(phoneNumber)") {
            let application:UIApplication = UIApplication.shared
            if (application.canOpenURL(phoneCallURL)) {
                application.open(phoneCallURL, options: [:], completionHandler: nil)
            }
        }
    }
    
    func showAlert(message:String) {
        let alert = UIAlertController(title: "Error!", message: message, preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "Ok", style: .default, handler: nil))
        self.present(alert, animated: true, completion: nil)
    }
    
    func getHandBook(handbook: NSDictionary) -> CCHandbook {
        let tmpHandbook = CCHandbook()
        
        if let id = handbook["id"] as? String {
            tmpHandbook.id = id
        }
        if let handbook_file = handbook["handbook_file"] as? String {
            tmpHandbook.handbook_file = handbook_file
        }
        if let status = handbook["status"] as? String {
            tmpHandbook.status = status
        }
        if let date_on = handbook["date_on"] as? String {
            tmpHandbook.date_on = date_on
        }
        return tmpHandbook
    }
    
    func requestHandBook() {
        SVProgressHUD.show()
        ChildCareService.sharedInstance.getTraining(onSuccess: { (response) in
            debugPrint(response)
            SVProgressHUD.dismiss()
            if let result = response.result.value as? NSDictionary{
                if let status = result["status"] as? Bool {
                    if status == true {
                        if let handbookItems = result["result"] as? [NSDictionary] {
                            var tmpHandbooksArray = [CCHandbook]()
                            for handbook in handbookItems {
                                tmpHandbooksArray.append(self.getHandBook(handbook: handbook))
                            }
                            self.handbooksArray.removeAll(keepingCapacity: false)
                            self.handbooksArray = tmpHandbooksArray
                            
                            let url : URL! = URL(string: self.handbooksArray[0].handbook_file!)
                            self.webView.loadRequest(URLRequest(url: url))
                        }
                    } else {
                        if let message = result["message"] as? String {
                            self.showAlert(message: message)
                            return
                        }
                    }
                }
            }
            }, onFailure: { (error) in
                debugPrint(error)
                SVProgressHUD.dismiss()
                self.showAlert(message: error.localizedDescription)
        })
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

}
