//
//  CCFormsViewController.swift
//  ChildCare
//
//  Created by Mac on 08/02/17.
//  Copyright © 2017 Edward. All rights reserved.
//

import UIKit

class CCFormsViewController: UIViewController {
    
    @IBOutlet weak var formsTableView: UITableView!
    
    var formsArray = ["Excursion risk management plan", "Regular outing permission form", "Excursion permission form"]

    override func viewDidLoad() {
        super.viewDidLoad()
        
        formsTableView.estimatedRowHeight = 100
        formsTableView.rowHeight = UITableViewAutomaticDimension
        
        navigationItem.title = "Forms"
        
        addLeftNavBar()
    }
    
    func addLeftNavBar() {
        let letfButton = UIButton()
        letfButton.setImage(UIImage(named: "back_icon"), for: .normal)
        letfButton.frame = CGRect(x:-40, y:0, width:30, height:30)
        letfButton.imageEdgeInsets = UIEdgeInsetsMake(0, -20, 0, 0)
        letfButton.addTarget(self, action: #selector(back), for: .touchUpInside)
        self.navigationItem.setLeftBarButton(UIBarButtonItem(customView: letfButton), animated: true);
        
        let rightButton = UIButton()
        rightButton.setImage(UIImage(named: "contact-icon"), for: .normal)
        rightButton.frame = CGRect(x:0, y:0, width:72, height:30)
        rightButton.imageEdgeInsets = UIEdgeInsetsMake(3, 0, 0, -10)
        rightButton.addTarget(self, action: #selector(openContact), for: .touchUpInside)
        self.navigationItem.setRightBarButton(UIBarButtonItem(customView: rightButton), animated: true);
    }
    func openContact() {
        let mobileNumber = CCUserInfoManager.sharedInstance.getCurrentUser()?.mobile
        self.callNumber(phoneNumber: mobileNumber!)
    }
    private func callNumber(phoneNumber:String) {
        if let phoneCallURL = URL(string: "tel://\(phoneNumber)") {
            let application:UIApplication = UIApplication.shared
            if (application.canOpenURL(phoneCallURL)) {
                application.open(phoneCallURL, options: [:], completionHandler: nil)
            }
        }
    }
    func back() {
        _ = self.navigationController?.popViewController(animated: true)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

}
extension CCFormsViewController: UITableViewDataSource, UITableViewDelegate {
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.formsArray.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        var cell = UITableViewCell()
        let _cell = tableView.dequeueReusableCell(withIdentifier: "CCFormCell", for: indexPath) as! CCFormCell
        _cell.lblFormName.text = self.formsArray[indexPath.row]
        cell = _cell
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableViewAutomaticDimension
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        if indexPath.row == 0 {
            performSegue(withIdentifier: "showRiskSegue", sender: self)
        } else if indexPath.row == 1 {
            performSegue(withIdentifier: "showRegularSegue", sender: self)
        } else if indexPath.row == 2 {
            performSegue(withIdentifier: "showPermissionSegue", sender: self)
        }
    }
}
