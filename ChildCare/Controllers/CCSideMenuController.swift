//
//  CCSideMenuController.swift
//  ChildCare
//
//  Created by Mac on 06/02/17.
//  Copyright © 2017 Edward. All rights reserved.
//

import UIKit
import SideMenuController

class CCSideMenuController: SideMenuController {

    override func viewDidLoad() {
        super.viewDidLoad()
        performSegue(withIdentifier: "showHomeController", sender: nil)
        performSegue(withIdentifier: "containSideMenu", sender: nil)
    }

}
