//
//  CCExcursionRiskViewController.swift
//  ChildCare
//
//  Created by Mac on 21/02/17.
//  Copyright © 2017 Edward. All rights reserved.
//

import UIKit
import SVProgressHUD

class CCExcursionRiskViewController: UITableViewController {
    
    @IBOutlet weak var dats: UITextField!
    @IBOutlet weak var destination: UITextField!
    @IBOutlet weak var departure: UITextField!
    
    @IBOutlet weak var btnYes: UIButton!
    @IBOutlet weak var btnNo: UIButton!
    
    @IBOutlet weak var riskAssignment: UITextField!
    @IBOutlet weak var transport: UITextField!
    @IBOutlet weak var name_coordinator: UITextField!
    @IBOutlet weak var bh_number: UITextField!
    @IBOutlet weak var m_number: UITextField!
    @IBOutlet weak var numChild: UITextField!
    @IBOutlet weak var numEducator: UITextField!
    @IBOutlet weak var ratio: UITextField!
    
    @IBOutlet weak var picker: UIPickerView!
    
    @IBOutlet weak var listTextField: UITextField!
    
    @IBOutlet weak var btnSubmit: UIButton!
    
    var isWaterSelected: Bool?
    var isOtherSelected: Bool?
    
    var selectedRadioStr: String! = "no"
    var checkString: String?
    var excursionCheckStr: String?
    
    var pickerData:[String] = ["First aid kit",
                               "List of edults participating in the excursion",
                               "List of children attending the excurtion",
                               "Contact information of each adult",
                               "Contact information for each child",
                               "Mobile phone / other means of communication with the service & emergency services",
                               "Medical information for each child",
                               "Other items"]
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        navigationItem.title = "Form Detail"
        
        tableView.estimatedRowHeight = 100
        tableView.rowHeight = UITableViewAutomaticDimension
        
        addLeftNavBar()
        
        isWaterSelected = false
        isOtherSelected = false
        self.selectButton(radioButton: self.btnNo)
        
        initUI()
        
    }
    
    func initUI() {
        setBorder(textField: dats)
        setBorder(textField: destination)
        setBorder(textField: departure)
        setBorder(textField: riskAssignment)
        setBorder(textField: transport)
        setBorder(textField: name_coordinator)
        setBorder(textField: bh_number)
        setBorder(textField: m_number)
        setBorder(textField: numChild)
        setBorder(textField: numEducator)
        setBorder(textField: ratio)
        setBorder(textField: listTextField)
        
        self.btnSubmit.layer.cornerRadius = 24
        
        self.picker.selectRow(0, inComponent: 0, animated: true)
        excursionCheckStr = pickerData[0]
    }
    
    func setBorder(textField: UITextField) {
        textField.layer.borderWidth = 1
        textField.layer.borderColor = UIColor.darkGray.cgColor
    }
    
    func addLeftNavBar() {
        let letfButton = UIButton()
        letfButton.setImage(UIImage(named: "back_icon"), for: .normal)
        letfButton.frame = CGRect(x:-40, y:0, width:30, height:30)
        letfButton.imageEdgeInsets = UIEdgeInsetsMake(0, -20, 0, 0)
        letfButton.addTarget(self, action: #selector(back), for: .touchUpInside)
        self.navigationItem.setLeftBarButton(UIBarButtonItem(customView: letfButton), animated: true);
        
        let rightButton = UIButton()
        rightButton.setImage(UIImage(named: "contact-icon"), for: .normal)
        rightButton.frame = CGRect(x:0, y:0, width:72, height:30)
        rightButton.imageEdgeInsets = UIEdgeInsetsMake(3, 0, 0, -10)
        rightButton.addTarget(self, action: #selector(openContact), for: .touchUpInside)
        self.navigationItem.setRightBarButton(UIBarButtonItem(customView: rightButton), animated: true);
    }
    func openContact() {
        let mobileNumber = CCUserInfoManager.sharedInstance.getCurrentUser()?.mobile
        self.callNumber(phoneNumber: mobileNumber!)
    }
    private func callNumber(phoneNumber:String) {
        if let phoneCallURL = URL(string: "tel://\(phoneNumber)") {
            let application:UIApplication = UIApplication.shared
            if (application.canOpenURL(phoneCallURL)) {
                application.open(phoneCallURL, options: [:], completionHandler: nil)
            }
        }
    }
    func back() {
        _ = self.navigationController?.popViewController(animated: true)
    }
    
    func selectButton(radioButton: UIButton) {
        let checkImage = UIImage(named: "check-box")
        let uncheckImage = UIImage(named: "uncheck-box")
        
        if radioButton == btnYes {
            btnYes.setBackgroundImage(checkImage, for: .normal)
            btnNo.setBackgroundImage(uncheckImage, for: .normal)
            selectedRadioStr = "yes"
            isWaterSelected = true
        } else if radioButton == btnNo {
            btnNo.setBackgroundImage(checkImage, for: .normal)
            btnYes.setBackgroundImage(uncheckImage, for: .normal)
            selectedRadioStr = "no"
            isWaterSelected = false
        }
        
        self.tableView.reloadData()
    }
    
    @IBAction func onClickYes(_ sender: AnyObject) {
        self.selectButton(radioButton: btnYes)
    }
    @IBAction func onClickNo(_ sender: AnyObject) {
        self.selectButton(radioButton: btnNo)
    }
    
    func showAlert(title:String, message:String) {
        let alert = UIAlertController(title: title, message: message, preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "Ok", style: .default, handler: nil))
        self.present(alert, animated: true, completion: nil)
    }
    
    @IBAction func onClickSubmit(_ sender: AnyObject) {
        let educator_id = CCUserInfoManager.sharedInstance.getCurrentUser()?.id
        let excurstion_destination = destination.text!
        let date_excursion = dats.text!
        let departure_arrive_times = departure.text!
        let water_hazards = selectedRadioStr!
        var risk_assessment = ""
        if water_hazards == "yes" {
            risk_assessment = riskAssignment.text!
        }
        
        let method_transport = transport.text!
        let excursion_coordinator = name_coordinator.text!
        let contactnumber_BH = bh_number.text!
        let contactnumber_M = m_number.text!
        let number_children = numChild.text!
        let number_educators = numEducator.text!
        let educator_child_ratio = ratio.text!
        let excursion_checklist = excursionCheckStr!
        
        var other_detail = ""
        if isOtherSelected! {
            other_detail = listTextField.text!
        }
        let created_location = ""
        SVProgressHUD.show()
        ChildCareService.sharedInstance.postRiskForm(educator_id:educator_id!,
                                                     excurstion_destination: excurstion_destination,
                                                     date_excursion: date_excursion,
                                                     departure_arrive_times: departure_arrive_times,
                                                     water_hazards: water_hazards,
                                                     risk_assessment: risk_assessment,
                                                     method_transport: method_transport,
                                                     excursion_coordinator: excursion_coordinator,
                                                     contactnumber_BH: contactnumber_BH,
                                                     contactnumber_M: contactnumber_M,
                                                     number_children: number_children,
                                                     number_educators: number_educators,
                                                     educator_child_ratio: educator_child_ratio,
                                                     excursion_checklist: excursion_checklist,
                                                     other_detail: other_detail,
                                                     created_location: created_location,
                                                     onSuccess: { (response) in
            SVProgressHUD.dismiss()
            if let result = response.result.value as? NSDictionary{
                if let status = result["status"] as? Bool {
                    if status == true {
                        self.showAlert(title:"ChildCare", message: "Posted successfully!")
                    } else {
                        if let message = result["message"] as? String {
                            self.showAlert(title:"Error!", message: message)
                            return
                        }
                    }
                }
            }
            }, onFailure: { (error) in
                SVProgressHUD.dismiss()
                self.showAlert(title:"Error!", message: error.localizedDescription)
        })
    }
    
    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if !isWaterSelected! && isOtherSelected! {
            if indexPath.row == 4 {
                return 0.01
            }
            return UITableViewAutomaticDimension
        }
        else if !isWaterSelected! && !isOtherSelected! {
            if indexPath.row == 4 || indexPath.row == 13 {
                return 0.01
            }
            return UITableViewAutomaticDimension
        } else if isWaterSelected! && isOtherSelected! {
            
            return UITableViewAutomaticDimension

        } else if isWaterSelected! && !isOtherSelected! {
            if indexPath.row == 13 {
                return 0.01
            }
            return UITableViewAutomaticDimension
            
        }
        return UITableViewAutomaticDimension
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

}

extension CCExcursionRiskViewController:UIPickerViewDelegate, UIPickerViewDataSource {
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return pickerData.count
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        return pickerData[row]
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        if row == pickerData.count-1 {
            isOtherSelected = true
        } else {
            isOtherSelected = false
        }
        excursionCheckStr = pickerData[row]
        self.tableView.reloadData()
    }
}
