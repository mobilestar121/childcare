//
//  CCPermissionViewController.swift
//  ChildCare
//
//  Created by Mac on 22/02/17.
//  Copyright © 2017 Edward. All rights reserved.
//

import UIKit
import SVProgressHUD

class CCPermissionViewController: UITableViewController {
    
    @IBOutlet weak var educatorName: UITextField!
    @IBOutlet weak var dateOfExcursion: UITextField!
    @IBOutlet weak var nameOfChild: UITextField!
    @IBOutlet weak var reasonOfOuting: UITextField!
    @IBOutlet weak var timeOfLeave: UITextField!
    @IBOutlet weak var timeOfArrive: UITextField!
    @IBOutlet weak var numChildren: UITextField!
    @IBOutlet weak var ratio: UITextField!
    
    @IBOutlet weak var picker: UIPickerView!
    
    @IBOutlet weak var other: UITextField!
    @IBOutlet weak var nameStaff: UITextField!
    
    @IBOutlet weak var me: UITextField!
    @IBOutlet weak var givePermission: UITextField!
    @IBOutlet weak var participate: UITextField!
    
    @IBOutlet weak var phoneNumber: UITextField!
    @IBOutlet weak var mobileNumber: UITextField!
    
    @IBOutlet weak var parentEducatorName: UITextField!
    
    @IBOutlet weak var measureSignature: SignatureView!
    
    @IBOutlet weak var btnSubmit: UIButton!
    
    var pickerData:[String] = ["By Car",
                               "By Bus",
                               "By Taxi",
                               "By train",
                               "By Tram",
                               "By Walking",
                               "Other"]
    
    var transportStr: String?
    var isOtherSelected: Bool?
    var signBaseString: String?

    override func viewDidLoad() {
        super.viewDidLoad()

        navigationItem.title = "Form Detail"
        
        tableView.estimatedRowHeight = 100
        tableView.rowHeight = UITableViewAutomaticDimension
        
        addLeftNavBar()
        
        isOtherSelected = false
        
        initUI()
    }
    
    func initUI() {
        setBorderTextField(textField: educatorName)
        setBorderTextField(textField: dateOfExcursion)
        setBorderTextField(textField: nameOfChild)
        setBorderTextField(textField: reasonOfOuting)
        setBorderTextField(textField: timeOfLeave)
        setBorderTextField(textField: timeOfArrive)
        setBorderTextField(textField: numChildren)
        setBorderTextField(textField: ratio)
        setBorderTextField(textField: other)
        setBorderTextField(textField: nameStaff)
        setBorderTextField(textField: me)
        setBorderTextField(textField: givePermission)
        setBorderTextField(textField: participate)
        setBorderTextField(textField: phoneNumber)
        setBorderTextField(textField: mobileNumber)
        setBorderTextField(textField: parentEducatorName)
                
        self.btnSubmit.layer.cornerRadius = 24
        
        self.picker.selectRow(0, inComponent: 0, animated: true)
        transportStr = pickerData[0]
    }
    func setBorderTextField(textField: UITextField) {
        textField.layer.borderWidth = 1
        textField.layer.borderColor = UIColor.darkGray.cgColor
    }
    
    func addLeftNavBar() {
        let letfButton = UIButton()
        letfButton.setImage(UIImage(named: "back_icon"), for: .normal)
        letfButton.frame = CGRect(x:-40, y:0, width:30, height:30)
        letfButton.imageEdgeInsets = UIEdgeInsetsMake(0, -20, 0, 0)
        letfButton.addTarget(self, action: #selector(back), for: .touchUpInside)
        self.navigationItem.setLeftBarButton(UIBarButtonItem(customView: letfButton), animated: true);
        
        let rightButton = UIButton()
        rightButton.setImage(UIImage(named: "contact-icon"), for: .normal)
        rightButton.frame = CGRect(x:0, y:0, width:72, height:30)
        rightButton.imageEdgeInsets = UIEdgeInsetsMake(3, 0, 0, -10)
        rightButton.addTarget(self, action: #selector(openContact), for: .touchUpInside)
        self.navigationItem.setRightBarButton(UIBarButtonItem(customView: rightButton), animated: true);
    }
    func openContact() {
        let mobileNumber = CCUserInfoManager.sharedInstance.getCurrentUser()?.mobile
        self.callNumber(phoneNumber: mobileNumber!)
    }
    private func callNumber(phoneNumber:String) {
        if let phoneCallURL = URL(string: "tel://\(phoneNumber)") {
            let application:UIApplication = UIApplication.shared
            if (application.canOpenURL(phoneCallURL)) {
                application.open(phoneCallURL, options: [:], completionHandler: nil)
            }
        }
    }
    func back() {
        _ = self.navigationController?.popViewController(animated: true)
    }
    
    func showAlert(title:String, message:String) {
        let alert = UIAlertController(title: title, message: message, preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "Ok", style: .default, handler: nil))
        self.present(alert, animated: true, completion: nil)
    }
    
    @IBAction func onClickSubmit(_ sender: AnyObject) {
        let signatureImage = measureSignature.getSignature()
        let signData = UIImageJPEGRepresentation(signatureImage, 0.1)
        signBaseString = signData?.base64EncodedString(options: .endLineWithCarriageReturn)
        
        let educator_id = CCUserInfoManager.sharedInstance.getCurrentUser()?.id
        
        let educators_full_name = educatorName.text!
        let dats_excursion = dateOfExcursion.text!
        let name_child = nameOfChild.text!
        let reason_excursion = reasonOfOuting.text!
        let time_leave = timeOfLeave.text!
        let time_arrive = timeOfArrive.text!
        let number_children_attending_outing = numChildren.text!
        let ratio_educators_children = ratio.text!
        var method_transport = ""
        if isOtherSelected! {
            method_transport = transportStr!
        }
        let other_detail = other.text!
        
        let other_staff_adult_accomanying_supervising = nameStaff.text!
        let i_permission = me.text!
        let permission_child_children = givePermission.text!
        let particpate_outing_educator = participate.text!
        
        let phone_no = phoneNumber.text!
        let mobile = mobileNumber.text!
        let parent_gurdian_name = parentEducatorName.text!
        let siganture = signBaseString
        let created_location = ""
        
        SVProgressHUD.show()
        ChildCareService.sharedInstance.postExcusionForm(educator_id: educator_id!,
                                                        educators_full_name: educators_full_name,
                                                        dats_excursion: dats_excursion,
                                                        name_child: name_child,
                                                        reason_excursion: reason_excursion,
                                                        time_leave: time_leave,
                                                        time_arrive: time_arrive,
                                                        number_children_attending_outing: number_children_attending_outing,
                                                        ratio_educators_children: ratio_educators_children,
                                                        method_transport: method_transport,
                                                        other_detail: other_detail,
                                                        other_staff_adult_accomanying_supervising: other_staff_adult_accomanying_supervising,
                                                        i_permission: i_permission,
                                                        permission_child_children: permission_child_children,
                                                        particpate_outing_educator: particpate_outing_educator,
                                                        phone_no: phone_no,
                                                        mobile: mobile,
                                                        parent_gurdian_name: parent_gurdian_name,
                                                        siganture: siganture!,
                                                        created_location: created_location,
                                                        onSuccess: { (response) in
                                                            SVProgressHUD.dismiss()
                                                            if let result = response.result.value as? NSDictionary{
                                                                if let status = result["status"] as? Bool {
                                                                    if status == true {
                                                                        self.showAlert(title:"ChildCare", message: "Posted successfully!")
                                                                    } else {
                                                                        if let message = result["message"] as? String {
                                                                            self.showAlert(title:"Error!", message: message)
                                                                            return
                                                                        }
                                                                    }
                                                                }
                                                            }
            }, onFailure: { (error) in
                SVProgressHUD.dismiss()
                self.showAlert(title:"Error!", message: error.localizedDescription)
        })
        
    }
    
    @IBAction func onClickMesureSignature(_ sender: AnyObject) {
        if tableView.isScrollEnabled {
            tableView.isScrollEnabled = false
        } else {
            tableView.isScrollEnabled = true
        }
    }
    
    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if !isOtherSelected! {
            if indexPath.row == 9{
                return 0.01
            }
        }
        
        return UITableViewAutomaticDimension
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

}

extension CCPermissionViewController:UIPickerViewDelegate, UIPickerViewDataSource {
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return pickerData.count
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        return pickerData[row]
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        if row == pickerData.count-1 {
            isOtherSelected = true
        } else {
            isOtherSelected = false
        }
        transportStr = pickerData[row]
        self.tableView.reloadData()
    }
}
