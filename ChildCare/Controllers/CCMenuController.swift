//
//  CCMenuController.swift
//  ChildCare
//
//  Created by Mac on 06/02/17.
//  Copyright © 2017 Edward. All rights reserved.
//

import UIKit

class CCMenuController: UITableViewController {
    
    let segues = [["icon": "home",       "name": "Home",      "segue": "showHomeController"],
                  ["icon": "training",       "name": "Training",      "segue": "showTrainingController"],
                  ["icon": "ic_hand",       "name": "Handbook",      "segue": "showHandbookController"],
                ["icon": "contact",       "name": "Contact",      "segue": "showContactController"],
                  ["icon": "suggest",   "name": "Suggestions",  "segue": "showSuggestController"],
                  ["icon": "resource",     "name": "Resources",    "segue": "showResourceController"],
                  ["icon": "signout",       "name": "Sign Out",     "segue": ""]]
    
    private var previousIndex: NSIndexPath?
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return segues.count
    }
    
    override func tableView(_ tableView: UITableView,
                            cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "CCMenuCell", for: indexPath) as! CCMenuCell
        
        cell.lblTitle.font = UIFont(name: "HelveticaNeue-Light", size: 15)
        cell.lblTitle.text = segues[indexPath.row]["name"]
        
        cell.imgIcon.image = UIImage(named: segues[indexPath.row]["icon"]!)
        
        return cell
    }
    
    override func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let cell = self.tableView.dequeueReusableCell(withIdentifier: "CCProfileHeaderCell") as! CCProfileHeaderCell
        cell.lblName.text = "\((CCUserInfoManager.sharedInstance.getCurrentUser()?.coach_name)!) \((CCUserInfoManager.sharedInstance.getCurrentUser()?.coach_lname)!)"
        cell.configureCell(with: (CCUserInfoManager.sharedInstance.getCurrentUser()?.profile_pic)!, placeholderImage: UIImage(named: "photo")!)
        
        return cell.contentView
    }
    override func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 136.0
    }
    
    override func tableView(_ tableView: UITableView,
                            didSelectRowAt indexPath: IndexPath)  {
        
        if let index = previousIndex {
            tableView.deselectRow(at: index as IndexPath, animated: true)
        }
        if indexPath.row != (segues.count-1) {
            sideMenuController?.performSegue(withIdentifier: segues[indexPath.row]["segue"]!, sender: nil)
        } else {
            CCUserInfoManager.sharedInstance.removeCurrentUser()
            
            let appDelegate = UIApplication.shared.delegate as! AppDelegate
            appDelegate.logout()
        }
        
        previousIndex = indexPath as NSIndexPath?
    }

}
