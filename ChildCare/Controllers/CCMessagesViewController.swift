//
//  CCMessagesViewController.swift
//  ChildCare
//
//  Created by Mac on 17/02/17.
//  Copyright © 2017 Edward. All rights reserved.
//

import UIKit
import JSQMessagesViewController
import SVProgressHUD

struct User {
    let id: String
    let name: String
}

class CCMessagesViewController: JSQMessagesViewController {
    let user1 = User(id: "1", name: "Steve")
    let user2 = User(id: "2", name: "Tim")
    
    
    var currentUser: User {
        return user1
    }
    
    // all messages of users1, users2
    var messages = [JSQMessage]()
}

extension CCMessagesViewController {

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        navigationItem.title = "Messages"
        
        addLeftNavBar()
        
        let currentUser = CCUserInfoManager.sharedInstance.getCurrentUser()
        
        
        self.senderId = (currentUser?.id)!
        self.senderDisplayName = (currentUser?.coach_name)!
        
        
        getMessages()
    }
    
    func addLeftNavBar() {
        let letfButton = UIButton()
        letfButton.setImage(UIImage(named: "back_icon"), for: .normal)
        letfButton.frame = CGRect(x:-40, y:0, width:30, height:30)
        letfButton.imageEdgeInsets = UIEdgeInsetsMake(0, -20, 0, 0)
        letfButton.addTarget(self, action: #selector(back), for: .touchUpInside)
        self.navigationItem.setLeftBarButton(UIBarButtonItem(customView: letfButton), animated: true);
        
        let rightButton = UIButton()
        rightButton.setImage(UIImage(named: "contact-icon"), for: .normal)
        rightButton.frame = CGRect(x:0, y:0, width:72, height:30)
        rightButton.imageEdgeInsets = UIEdgeInsetsMake(3, 0, 0, -10)
        rightButton.addTarget(self, action: #selector(openContact), for: .touchUpInside)
        self.navigationItem.setRightBarButton(UIBarButtonItem(customView: rightButton), animated: true);
    }
    func openContact() {
        let mobileNumber = CCUserInfoManager.sharedInstance.getCurrentUser()?.mobile
        self.callNumber(phoneNumber: mobileNumber!)
    }
    private func callNumber(phoneNumber:String) {
        if let phoneCallURL = URL(string: "tel://\(phoneNumber)") {
            let application:UIApplication = UIApplication.shared
            if (application.canOpenURL(phoneCallURL)) {
                application.open(phoneCallURL, options: [:], completionHandler: nil)
            }
        }
    }
    func back() {
        _ = self.navigationController?.popViewController(animated: true)
    }
    
    func showAlert(title:String, message:String) {
        let alert = UIAlertController(title: title, message: message, preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "Ok", style: .default, handler: nil))
        self.present(alert, animated: true, completion: nil)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

}
extension CCMessagesViewController {
    func getMessage(message: NSDictionary) -> CCMessage {
        let tmpMessage = CCMessage()
        
        if let sender_id = message["sender_id"] as? String {
            tmpMessage.sender_id = sender_id
        }
        if let message = message["message"] as? String {
            tmpMessage.message = message
        }
        if let message_date = message["message_date"] as? String {
            tmpMessage.message_date = message_date
        }
        return tmpMessage
    }
    
    func convertString2Date(dateString:String) -> Date {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
        let date = dateFormatter.date(from: dateString)
        return date!
    }
    
    func getMessages() {
        var msgs = [JSQMessage]()
        
//        let message1 = JSQMessage(senderId: "1", displayName: "Steve", text: "Hey Tim how are you?")
//        let message2 = JSQMessage(senderId: "2", displayName: "Tim", text: "Fine thanks, and you?")
//        
//        messages.append(message1!)
//        messages.append(message2!)
        
        SVProgressHUD.show()
        
        let educatorId = CCUserInfoManager.sharedInstance.getCurrentUser()?.id
        ChildCareService.sharedInstance.getMessageList(educatorId: educatorId!, onSuccess: { (response) in
            SVProgressHUD.dismiss()
            if let result = response.result.value as? NSDictionary{
                if let status = result["status"] as? Bool {
                    if status == true {
                        if let messageItems = result["result"] as? [NSDictionary] {
                            for messageItem in messageItems {
                                let message = self.getMessage(message: messageItem)
                                var senderName = ""
                                if (message.sender_id == "1") {
                                    senderName = "Admin"
                              } else {
//                                    senderName = (CCUserInfoManager.sharedInstance.getCurrentUser()?.coach_name)!
                                    senderName = "Me"
                                }
                                msgs.append(JSQMessage(senderId: message.sender_id!, senderDisplayName: senderName, date: self.convertString2Date(dateString: message.message_date!), text: message.message!))
                            }
                            
                            self.messages = msgs                            
                            self.collectionView.reloadData()
                            let item = self.collectionView(self.collectionView!, numberOfItemsInSection: 0) - 1
                            let lastItemIndex = IndexPath.init(row: item, section: 0)
                            self.collectionView?.scrollToItem(at: lastItemIndex, at: .top, animated: false)
                        }
                    } else {
                        if let err = result["error"] as? NSDictionary {
                            if let message = err["message"] as? String {
                                self.showAlert(title:"Error!", message: message)
                                return
                            }
                        }
                    }
                }
            }
            }, onFailure: { (error) in
                SVProgressHUD.dismiss()
                self.showAlert(title:"Error!", message: error.localizedDescription)
                return
        })
    }
    
    
}

extension CCMessagesViewController {
    
    override func didPressSend(_ button: UIButton!, withMessageText text: String!, senderId: String!, senderDisplayName: String!, date: Date!) {
        let educatorId = CCUserInfoManager.sharedInstance.getCurrentUser()?.id
        var message = text.replacingOccurrences(of: " ", with: "%20")
        message = message.replacingOccurrences(of: "\n", with: "%0D%0A")
        ChildCareService.sharedInstance.sendMessage(educatorId: educatorId!, message:message, onSuccess: { (response) in
            SVProgressHUD.dismiss()
            if let result = response.result.value as? NSDictionary{
                if let status = result["status"] as? Bool {
                    if status == true {
                        if let messageItem = result["result"] as? NSDictionary {
                            let message = self.getMessage(message: messageItem)
                            var senderName = ""
                            if (message.sender_id == "1") {
                                senderName = "Admin"
                            } else {
                                //senderName = (CCUserInfoManager.sharedInstance.getCurrentUser()?.coach_name)!
                                senderName = "Me"
                            }
                            let msg = JSQMessage(senderId: message.sender_id!, senderDisplayName: senderName, date: self.convertString2Date(dateString: message.message_date!), text: message.message!)
                            self.messages.append(msg!)
                            
                            self.finishSendingMessage()
                        }
                    } else {
                        if let message = result["message"] as? String {
                            self.showAlert(title:"Error!", message: message)
                            return
                        }
                    }
                }
            }
            }, onFailure: { (error) in
                SVProgressHUD.dismiss()
                self.showAlert(title:"Error!", message: error.localizedDescription)
                return
        })
        
        
        
    }
    
    override func collectionView(_ collectionView: JSQMessagesCollectionView!, layout collectionViewLayout: JSQMessagesCollectionViewFlowLayout!, heightForCellTopLabelAt indexPath: IndexPath!) -> CGFloat {
        if indexPath.item == 0 {
            return kJSQMessagesCollectionViewCellLabelHeightDefault
        }
        if (indexPath.item - 1 > 0) {
            let previousMessage = self.messages[indexPath.item - 1]
            let message = self.messages[indexPath.item]
            if message.date.timeIntervalSince(previousMessage.date) / 60 > 1 {
                return kJSQMessagesCollectionViewCellLabelHeightDefault
            }
        }
        return 0.0
    }
    
    override func collectionView(_ collectionView: JSQMessagesCollectionView!, attributedTextForMessageBubbleTopLabelAt indexPath: IndexPath!) -> NSAttributedString! {
        let nameAttributes = [NSFontAttributeName: UIFont.systemFont(ofSize: 15)]
        let message = messages[indexPath.row]
        let messageUsername = message.senderDisplayName + "   "
        
        let combination = NSMutableAttributedString()
        
        combination.append(NSAttributedString(string: messageUsername, attributes: nameAttributes))
        combination.append(JSQMessagesTimestampFormatter.shared().attributedTimestamp(for: message.date)!)
        
        return combination
    }
    
    override func collectionView(_ collectionView: JSQMessagesCollectionView!, layout collectionViewLayout: JSQMessagesCollectionViewFlowLayout!, heightForMessageBubbleTopLabelAt indexPath: IndexPath!) -> CGFloat {
        return 20
    }
    
    override func collectionView(_ collectionView: JSQMessagesCollectionView!, avatarImageDataForItemAt indexPath: IndexPath!) -> JSQMessageAvatarImageDataSource! {
        return nil
    }
    
    override func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = super.collectionView(collectionView, cellForItemAt: indexPath) as! JSQMessagesCollectionViewCell
        
        // messages to show
        let msg = messages[indexPath.row]
        
        if !msg.isMediaMessage {
            if currentUser.id == msg.senderId {
                cell.textView.textColor = UIColor.white
            }else{
                cell.textView.textColor = UIColor(netHex: 0x595968)
            }
            cell.textView.linkTextAttributes = [NSForegroundColorAttributeName: cell.textView.textColor ?? UIColor.white]
        }
        return cell
    }
    
    override func collectionView(_ collectionView: JSQMessagesCollectionView!, messageBubbleImageDataForItemAt indexPath: IndexPath!) -> JSQMessageBubbleImageDataSource! {
        let bubbleFactory = JSQMessagesBubbleImageFactory()
        
        let message = messages[indexPath.row]
        
        if currentUser.id == message.senderId {
            return bubbleFactory?.incomingMessagesBubbleImage(with: UIColor(netHex: 0xD2997A))
        } else {
            return bubbleFactory?.outgoingMessagesBubbleImage(with: UIColor(netHex: 0xF0F2F5))
        }
    }
    
    override func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return messages.count
    }
    
    override func collectionView(_ collectionView: JSQMessagesCollectionView!, messageDataForItemAt indexPath: IndexPath!) -> JSQMessageData! {
        return messages[indexPath.row]
    }
}

