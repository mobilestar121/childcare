//
//  ViewController.swift
//  ChildCare
//
//  Created by Mac on 06/02/17.
//  Copyright © 2017 Edward. All rights reserved.
//

import UIKit
import SideMenuController
import SVProgressHUD
	
class CCLoginViewController: UIViewController {
    @IBOutlet weak var txtEmail: UITextField!
    @IBOutlet weak var txtPassword: UITextField!
    
    @IBOutlet weak var btnLogin: UIButton!
    @IBOutlet weak var animView: UIView!

    override func viewDidLoad() {
        super.viewDidLoad()
        
        initUI()
    }
    
    func initUI() {
        self.txtEmail.backgroundColor = UIColor.clear
        self.txtPassword.backgroundColor = UIColor.clear
        self.view.layoutIfNeeded()
        self.view.setNeedsLayout()
        self.btnLogin.layer.cornerRadius = self.btnLogin.frame.size.height/2
        self.btnLogin.backgroundColor = UIColor(netHex: 0xe8c77a)
    }
    
    override func viewDidLayoutSubviews() {
        self.txtEmail.setBottomBorder(borderColor: UIColor.white)
        self.txtPassword.setBottomBorder(borderColor: UIColor.white)
    }
    
    func getUser(user: NSDictionary) -> CCUser {
        let tmpUser = CCUser()
        
        if let id = user["id"] as? String {
            tmpUser.id = id
        }
        if let coach_name = user["coach_name"] as? String {
            tmpUser.coach_name = coach_name
        }
        if let coach_lname = user["coach_lname"] as? String {
            tmpUser.coach_lname = coach_lname
        }
        if let mobile = user["mobile"] as? String {
            tmpUser.mobile = mobile
        }
        if let phone = user["phone"] as? String {
            tmpUser.phone = phone
        }
        if let email = user["email"] as? String {
            tmpUser.email = email
        }
        if let profile_pic = user["profile_pic"] as? String {
            tmpUser.profile_pic = profile_pic
        }
        
        
        return tmpUser
    }
    
    @IBAction func loginAction(_ sender: AnyObject) {
        
        self.txtEmail.text = "vijisa.smily@gmail.com"
        self.txtPassword.text = "Test@123"
        
        let userEmail = self.txtEmail.text!
        let userPass = self.txtPassword.text!
        
        if userEmail.isEmpty || userPass.isEmpty {
            self.showAlert(message: "Input Email or Password!")
            return
        }
        if !isValidEmail(testStr: userEmail) {
            self.showAlert(message: "Input Valid Email Address!")
            return
        }
        
        SVProgressHUD.show()
        ChildCareService.sharedInstance.login(emailStr: userEmail, passStr: userPass, onSuccess: { (response) in
            debugPrint(response)
            SVProgressHUD.dismiss()
            if let result = response.result.value as? NSDictionary{
                if let status = result["status"] as? Bool {
                    if status == true {
                        if let res = result["result"] as? NSDictionary {
                            let currentUser = self.getUser(user: res)
                            
                            CCUserInfoManager.sharedInstance.saveCurrentUser(user: currentUser)
                            self.performSegue(withIdentifier: "ShowHomeSegue", sender: self)
                            
                        }
                    } else {
                        if let message = result["message"] as? String {
                            self.showAlert(message: message)
                            return
                        }
                    }
                }
            }
            }, onFailure: { (error) in
                debugPrint(error)
                SVProgressHUD.dismiss()
                self.showAlert(message: error.localizedDescription)
        })
        
    }
    
    func isValidEmail(testStr:String) -> Bool {
        // print("validate calendar: \(testStr)")
        let emailRegEx = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,}"
        
        let emailTest = NSPredicate(format:"SELF MATCHES %@", emailRegEx)
        return emailTest.evaluate(with: testStr)
    }
    
    func showAlert(message:String) {
        let alert = UIAlertController(title: "Error!", message: message, preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "Ok", style: .default, handler: nil))
        self.present(alert, animated: true, completion: nil)
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "ShowHomeSegue"
        {
            if let destinationVC = segue.destination as? CCSideMenuController {
//                destinationVC.sessionId = self.sessionId
            }
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

}

