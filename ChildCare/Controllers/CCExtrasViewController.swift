//
//  CCExtrasViewController.swift
//  ChildCare
//
//  Created by Mac on 29/03/17.
//  Copyright © 2017 Edward. All rights reserved.
//

import UIKit
import DropDown
import SVProgressHUD

class CCExtrasViewController: UITableViewController {
    @IBOutlet weak var childContainer: UIView!
    @IBOutlet weak var typeContainer: UIView!
    @IBOutlet weak var extraContainer: UIView!
    @IBOutlet weak var quantityContainer: UIView!
    
    @IBOutlet weak var lblChild: UILabel!
    @IBOutlet weak var lblType: UILabel!
    @IBOutlet weak var lblMeal: UILabel!
    
    @IBOutlet weak var lblQuantity: UILabel!
    
    @IBOutlet weak var imgChildArrow: UIImageView!
    @IBOutlet weak var childIndicator: UIActivityIndicatorView!
    
    @IBOutlet weak var imgMealArrow: UIImageView!
    @IBOutlet weak var mealIndicator: UIActivityIndicatorView!
    
    @IBOutlet weak var btnSubmit: UIButton!
    
    let chooseChildDropDown = DropDown()
    let chooseTypeDropDown = DropDown()
    let chooseExtraDropDown = DropDown()
    
    var childrenArray = [CCExtraChild]()
    var extrasArray = [CCExtra]()
    var typesArray = [String]()
    
    var selectedChild: CCExtraChild?
    var selectedExtra: CCExtra?
    var selectedType: String?
    
    var isChildrenLoaded = false
    var isExtraLoaded = false
    var isTypeLoaded = false
    
    var quantity: Int! = 1

    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.childIndicator.isHidden = true
        self.mealIndicator.isHidden = true

        // Do any additional setup after loading the view.
        
        initUI()
        self.lblQuantity.text = "\(quantity!)"
    }
    
    func initUI() {
        self.childContainer.layer.borderWidth = 1.0
        self.childContainer.layer.borderColor = UIColor(netHex: 0xEEEEEE).cgColor
        self.childContainer.layer.cornerRadius = 2.0
        self.childContainer.layer.masksToBounds = true
        
        self.typeContainer.layer.borderWidth = 1.0
        self.typeContainer.layer.borderColor = UIColor(netHex: 0xEEEEEE).cgColor
        self.typeContainer.layer.cornerRadius = 2.0
        self.typeContainer.layer.masksToBounds = true
        
        self.extraContainer.layer.borderWidth = 1.0
        self.extraContainer.layer.borderColor = UIColor(netHex: 0xEEEEEE).cgColor
        self.extraContainer.layer.cornerRadius = 2.0
        self.extraContainer.layer.masksToBounds = true
        
        self.quantityContainer.layer.borderWidth = 1.0
        self.quantityContainer.layer.borderColor = UIColor(netHex: 0xEEEEEE).cgColor
        self.quantityContainer.layer.cornerRadius = 2.0
        self.quantityContainer.layer.masksToBounds = true
        
        self.btnSubmit.layer.cornerRadius = 24
    }
    
    func showAlert(title:String, message:String) {
        let alert = UIAlertController(title: title, message: message, preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "Ok", style: .default, handler: nil))
        self.present(alert, animated: true, completion: nil)
    }
    
    func getChild(child: NSDictionary) -> CCExtraChild {
        let tmpChild = CCExtraChild()
        
        if let id = child["id"] as? String {
            tmpChild.id = id
        }
        if let children_name = child["children_name"] as? String {
            tmpChild.children_name = children_name
        }
        if let parent_id = child["parent_id"] as? String {
            tmpChild.parent_id = parent_id
        }
        if let children_age = child["children_age"] as? String {
            tmpChild.children_age = children_age
        }
        if let children_gender = child["children_gender"] as? String {
            tmpChild.children_gender = children_gender
        }
        
        if let status = child["status"] as? String {
            tmpChild.status = status
        }
        if let date_on = child["date_on"] as? String {
            tmpChild.date_on = date_on
        }
        return tmpChild
    }
    
    func getChildren(onSuccess:@escaping (_ success: Bool) -> Void) {
        let educatorId = CCUserInfoManager.sharedInstance.getCurrentUser()?.id
        ChildCareService.sharedInstance.getExtraChildren(educatorId:educatorId!, onSuccess: { (response) in
            if let result = response.result.value as? NSDictionary{
                if let status = result["status"] as? Bool {
                    if status == true {
                        if let children = result["result"] as? [NSDictionary] {
                            var tmpChildrenArray = [CCExtraChild]()
                            for child in children {
                                tmpChildrenArray.append(self.getChild(child: child))
                            }
                            self.childrenArray.removeAll(keepingCapacity: false)
                            self.childrenArray = tmpChildrenArray
                            
                            self.isChildrenLoaded = true
                            
                            onSuccess(true)
                        }
                    } else {
                        onSuccess(false)
                        if let message = result["message"] as? String {
                            self.showAlert(title:"Error!", message: message)
                            return
                        }
                    }
                }
            }
            }, onFailure: { (error) in
                self.showAlert(title:"Error!", message: error.localizedDescription)
        })
    }
    
    func chooseChild() {
        chooseChildDropDown.show()
    }
    
    func setupChildDropDown(label:UILabel) {
        chooseChildDropDown.anchorView = label
        
        // Will set a custom with instead of anchor view width
        //		dropDown.width = 100
        
        // By default, the dropdown will have its origin on the top left corner of its anchor view
        // So it will come over the anchor view and hide it completely
        // If you want to have the dropdown underneath your anchor view, you can do this:
        chooseChildDropDown.bottomOffset = CGPoint(x: 0, y: label.bounds.height)
        
        // You can also use localizationKeysDataSource instead. Check the docs.
        var tmpChildrenTitelDataSource = [String] ()
        
        for child in self.childrenArray {
            tmpChildrenTitelDataSource.append(child.children_name!)
        }
        chooseChildDropDown.dataSource = tmpChildrenTitelDataSource
        
        // Action triggered on selection
        chooseChildDropDown.selectionAction = { (index, item) in
            label.text = item
            self.selectedChild = self.childrenArray[index]
        }
    }
    
    // Get Type
    func chooseType() {
        chooseTypeDropDown.show()
    }
    
    func setupTypeDropDown(label:UILabel) {
        chooseTypeDropDown.anchorView = label
        
        
        // Will set a custom with instead of anchor view width
        //		dropDown.width = 100
        
        // By default, the dropdown will have its origin on the top left corner of its anchor view
        // So it will come over the anchor view and hide it completely
        // If you want to have the dropdown underneath your anchor view, you can do this:
        chooseTypeDropDown.bottomOffset = CGPoint(x: 0, y: label.bounds.height)
        
        // You can also use localizationKeysDataSource instead. Check the docs.
        let typesTitelDataSource = ["Snack", "Breakfast", "Lunch", "Dinner"]
        
        chooseTypeDropDown.dataSource = typesTitelDataSource
        
        // Action triggered on selection
        chooseTypeDropDown.selectionAction = { (index, item) in
            label.text = item
            self.selectedType = typesTitelDataSource[index]
        }
    }
    
    // Get Extras
    
    func getExtra(extra: NSDictionary) -> CCExtra {
        let tmpExtra = CCExtra()
        
        if let id = extra["id"] as? String {
            tmpExtra.id = id
        }
        if let extras_name = extra["extras_name"] as? String {
            tmpExtra.extras_name = extras_name
        }
        if let extras_price = extra["extras_price"] as? String {
            tmpExtra.extras_price = extras_price
        }
        if let extras_cat = extra["extras_cat"] as? String {
            tmpExtra.extras_cat = extras_cat
        }
        
        if let status = extra["status"] as? String {
            tmpExtra.status = status
        }
        if let date_on = extra["date_on"] as? String {
            tmpExtra.date_on = date_on
        }
        return tmpExtra
    }
    
    func getExtras(onSuccess:@escaping (_ success: Bool) -> Void) {
        ChildCareService.sharedInstance.getExtraList(onSuccess: { (response) in
            if let result = response.result.value as? NSDictionary{
                if let status = result["status"] as? Bool {
                    if status == true {
                        if let extras = result["result"] as? [NSDictionary] {
                            var tmpExtrasArray = [CCExtra]()
                            for extra in extras {
                                tmpExtrasArray.append(self.getExtra(extra: extra))
                            }
                            self.extrasArray.removeAll(keepingCapacity: false)
                            self.extrasArray = tmpExtrasArray
                            
                            self.isExtraLoaded = true
                            
                            onSuccess(true)
                        }
                    } else {
                        onSuccess(false)
                        if let message = result["message"] as? String {
                            self.showAlert(title:"Error!", message: message)
                            return
                        }
                    }
                }
            }
            }, onFailure: { (error) in
                self.showAlert(title:"Error!", message: error.localizedDescription)
        })
    }
    
    func chooseExtra() {
        chooseExtraDropDown.show()
    }
    
    func setupExtraDropDown(label:UILabel) {
        chooseExtraDropDown.anchorView = label
        
        // Will set a custom with instead of anchor view width
        //		dropDown.width = 100
        
        // By default, the dropdown will have its origin on the top left corner of its anchor view
        // So it will come over the anchor view and hide it completely
        // If you want to have the dropdown underneath your anchor view, you can do this:
        chooseExtraDropDown.bottomOffset = CGPoint(x: 0, y: label.bounds.height)
        
        // You can also use localizationKeysDataSource instead. Check the docs.
        var tmpExtrasTitelDataSource = [String] ()
        
        for extra in self.extrasArray {
            tmpExtrasTitelDataSource.append(extra.extras_name!)
        }
        chooseExtraDropDown.dataSource = tmpExtrasTitelDataSource
        
        // Action triggered on selection
        chooseExtraDropDown.selectionAction = { (index, item) in
            label.text = item
            self.selectedExtra = self.extrasArray[index]
        }
    }
    
    
    @IBAction func onStepper(_ sender: UIStepper) {
        quantity = Int(sender.value)
        self.lblQuantity.text = "\(quantity!)"
    }
    
    @IBAction func onSubmit(_ sender: UIButton) {
        if selectedChild == nil {
            self.showAlert(title:"Error!", message: "Please select child")
            return
        }
        if selectedType == nil {
            self.showAlert(title:"Error!", message: "Please select type")
            return
        }
        if selectedExtra == nil {
            self.showAlert(title:"Error!", message: "Please select meal")
            return
        }
        
        let educatorId = CCUserInfoManager.sharedInstance.getCurrentUser()?.id
        
        SVProgressHUD.show()
        ChildCareService.sharedInstance.addOrderExtra(educatorId: educatorId!, extraId: (selectedExtra?.id)!, qtyId:"\(quantity!)", type: selectedType!, onSuccess: { (response) in
            SVProgressHUD.dismiss()
            if let result = response.result.value as? NSDictionary{
                if let status = result["status"] as? Bool {
                    if status == true {
                        self.showAlert(title: "ChildCare", message: "order success")
                    } else {
                        if let err = result["error"] as? NSDictionary {
                            if let message = err["message"] as? String {
                                self.showAlert(title:"Error!", message: message)
                                return
                            }
                        }
                    }
                }
            }
            }, onFailure: { (error) in
                SVProgressHUD.dismiss()
                self.showAlert(title:"Error!", message: error.localizedDescription)
        })
    }
    
    @IBAction func back() {
        _ = self.navigationController?.popViewController(animated: true)
    }
    
    func showChildIndicator(isHidden: Bool) {
        self.imgChildArrow.isHidden = !isHidden
        self.childIndicator.isHidden = isHidden
    }
    
    func showExtraIndicator(isHidden: Bool) {
        self.imgMealArrow.isHidden = !isHidden
        self.mealIndicator.isHidden = isHidden
    }
    
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if indexPath.row == 0 { // Select Child
            if !isChildrenLoaded {
                showChildIndicator(isHidden: false)
                
                self.childIndicator.startAnimating()
                self.getChildren(onSuccess: { (isSuccess) in
                    if isSuccess {
                        self.childIndicator.stopAnimating()
                        self.showChildIndicator(isHidden: true)
                        self.setupChildDropDown(label:self.lblChild)
                        
                        self.chooseChild()
                    }
                })
            } else {
                self.chooseChild()
            }
        } else if indexPath.row == 1 {
            if !isTypeLoaded {
                isTypeLoaded = true
                setupTypeDropDown(label: self.lblType)
            }
            
            self.chooseType()
        } else if indexPath.row == 2 { // Select Meal
            if !isExtraLoaded {
                showExtraIndicator(isHidden: false)
                
                self.mealIndicator.startAnimating()
                self.getExtras(onSuccess: { (isSuccess) in
                    if isSuccess {
                        self.mealIndicator.stopAnimating()
                        self.showExtraIndicator(isHidden: true)
                        self.setupExtraDropDown(label:self.lblMeal)
                        
                        self.chooseExtra()
                    }
                })
            } else {
                self.chooseExtra()
            }
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
