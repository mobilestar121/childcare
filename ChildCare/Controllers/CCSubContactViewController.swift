//
//  CCSubContactViewController.swift
//  ChildCare
//
//  Created by Mac on 24/02/17.
//  Copyright © 2017 Edward. All rights reserved.
//

import UIKit

class CCSubContactViewController: UIViewController {
    
    @IBOutlet weak var webView: UIWebView!

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        navigationItem.title = "Contact"
        
        addLeftNavBar()
        
        let url : URL! = URL(string: Constants.ChildCareAPI.CONTACT)
        self.webView.loadRequest(URLRequest(url: url))
    }
    
    func addLeftNavBar() {
        let letfButton = UIButton()
        letfButton.setImage(UIImage(named: "back_icon"), for: .normal)
        letfButton.frame = CGRect(x:-40, y:0, width:30, height:30)
        letfButton.imageEdgeInsets = UIEdgeInsetsMake(0, -20, 0, 0)
        letfButton.addTarget(self, action: #selector(back), for: .touchUpInside)
        self.navigationItem.setLeftBarButton(UIBarButtonItem(customView: letfButton), animated: true);
    }
    func back() {
        _ = self.navigationController?.popViewController(animated: true)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

}
