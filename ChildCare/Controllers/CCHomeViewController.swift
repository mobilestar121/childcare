//
//  CCHomeViewController.swift
//  ChildCare
//
//  Created by Mac on 06/02/17.
//  Copyright © 2017 Edward. All rights reserved.
//

import UIKit
import CoreLocation

class CCHomeViewController: CCContentViewController {

    @IBOutlet weak var lblDay: UILabel!
    @IBOutlet weak var btnDay: UIButton!
    var lattitude: String?
    var longitude: String?
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        navigationItem.title = "HOME"
        self.lattitude = ""
        self.longitude = ""
        
        addRightNavBar()
        
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        if UserDefaults.standard.bool(forKey: Constants.DAY_STARTED) {
            self.btnDay.backgroundColor = UIColor(netHex: 0xec723b)
            self.lblDay.text = "<<  Finish Day  >>"
        } else {
            self.btnDay.backgroundColor = UIColor(netHex: 0x0F3753)
            self.lblDay.text = "<<  Start Day  >>"
        }
        
        LocationService.sharedInstance.delegate = self
        getCurrentLocation()
    }
    
    func addRightNavBar() {
        let rightButton = UIButton()
        rightButton.setImage(UIImage(named: "contact-icon"), for: .normal)
        rightButton.frame = CGRect(x:0, y:0, width:72, height:30)
        rightButton.imageEdgeInsets = UIEdgeInsetsMake(3, 0, 0, -10)
        rightButton.addTarget(self, action: #selector(openContact), for: .touchUpInside)
        self.navigationItem.setRightBarButton(UIBarButtonItem(customView: rightButton), animated: true);
    }
    func openContact() {
        let mobileNumber = CCUserInfoManager.sharedInstance.getCurrentUser()?.mobile
        self.callNumber(phoneNumber: mobileNumber!)
    }
    private func callNumber(phoneNumber:String) {
        if let phoneCallURL = URL(string: "tel://\(phoneNumber)") {
            let application:UIApplication = UIApplication.shared
            if (application.canOpenURL(phoneCallURL)) {
                application.open(phoneCallURL, options: [:], completionHandler: nil)
            }
        }
    }
    
    func getCurrentLocation() {
        if CLLocationManager.locationServicesEnabled() {
            switch(CLLocationManager.authorizationStatus()) {
            case .notDetermined, .restricted, .denied:
                let title = "Turn On Location Services"
                let message = "Denied access Your Location:"
                let alert = UIAlertController(title: title, message: message, preferredStyle: .alert)
                alert.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler: nil))
                alert.addAction(UIAlertAction(title: "OK", style: .default) { (action) in
                    if let url = URL(string: UIApplicationOpenSettingsURLString) {
                        UIApplication.shared.open(url, options: [:], completionHandler: nil)
                    }
                })
                
                self.present(alert, animated: true, completion: nil)
                
                break
            case .authorizedAlways, .authorizedWhenInUse:
                print("Access")
                LocationService.sharedInstance.startUpdatingLocation()
                
                break
            }
        } else {
            print("Location services are not enabled")
            self.showAlert(title:"Error!", message: "Location services are not enabled")
        }
        
    }
    
    func showAlert(title:String, message:String) {
        let alert = UIAlertController(title: title, message: message, preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "Ok", style: .default, handler: nil))
        self.present(alert, animated: true, completion: nil)
    }

    @IBAction func onClickDay(_ sender: AnyObject) {
        let educatorId = CCUserInfoManager.sharedInstance.getCurrentUser()?.id
        
        if UserDefaults.standard.bool(forKey: Constants.DAY_STARTED) {
            ChildCareService.sharedInstance.finishDay(educatorId: educatorId!, lat: self.lattitude!, lon: self.longitude!, onSuccess: { (response) in
                if let result = response.result.value as? NSDictionary{
                    if let status = result["status"] as? Bool {
                        if status == true {
                            UserDefaults.standard.set(false, forKey: Constants.DAY_STARTED)
                            UserDefaults.standard.synchronize()
                            
                            self.lblDay.text = "<<  Start Day  >>"
                            self.btnDay.backgroundColor = UIColor(netHex: 0x0F3753)
                            self.showAlert(title: "ChildCare", message: "Finished Your Day!")
                        } else {
                            if let err = result["error"] as? NSDictionary {
                                if let message = err["message"] as? String {
                                    self.showAlert(title:"Error!", message: message)
                                    return
                                }
                            }
                        }
                    }
                }
                }, onFailure: { (error) in
                    self.showAlert(title:"Error!", message: error.localizedDescription)
            })
        } else {
            if self.lattitude == nil && self.longitude == nil {
                self.showAlert(title:"Error!", message: "Could not get location")
                return
            }
            ChildCareService.sharedInstance.startDay(educatorId: educatorId!, lat: self.lattitude!, lon: self.longitude!, onSuccess: { (response) in
                if let result = response.result.value as? NSDictionary{
                    if let status = result["status"] as? Bool {
                        if status == true {
                            UserDefaults.standard.set(true, forKey: Constants.DAY_STARTED)
                            UserDefaults.standard.synchronize()
                            
                            self.lblDay.text = "<<  Finish Day  >>"
                            self.btnDay.backgroundColor = UIColor(netHex: 0xec723b)
                            self.showAlert(title: "ChildCare", message: "Started Your Day!")
                        } else {
                            if let err = result["error"] as? NSDictionary {
                                if let message = err["message"] as? String {
                                    self.showAlert(title:"Error!", message: message)
                                    return
                                }
                            }
                        }
                    }
                }
                }, onFailure: { (error) in
                    self.showAlert(title:"Error!", message: error.localizedDescription)
            })
        }
    }
    
    
    @IBAction func onClickActions(_ sender: UIButton) {
        let tag = sender.tag
        switch tag {
        case 10: // activity
            if UserDefaults.standard.bool(forKey: Constants.DAY_STARTED) {
                performSegue(withIdentifier: "showActivitySegue", sender: self)
            } else {
                self.showAlert(title: "ChildCare", message: "For Activity Check-In you must first Start Day")
            }
            break
        case 11: // submit form
            performSegue(withIdentifier: "showFormSegue", sender: self)
            break
        case 12: // handbook
            performSegue(withIdentifier: "showExtraSegue", sender: self)
            break
        case 13: // programming
            performSegue(withIdentifier: "showProgramSegue", sender: self)
            break
        case 14: // news
            performSegue(withIdentifier: "showNewsSegue", sender: self)            
            break
        case 15: // messages
            performSegue(withIdentifier: "showMessageSegue", sender: self)            
            break
        default:
            break
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
}

extension CCHomeViewController: LocationServiceDelegate {
    func tracingLocation(currentLocation: CLLocation) {
        let lat = currentLocation.coordinate.latitude
        let lon = currentLocation.coordinate.longitude
        self.lattitude = String(format:"%.6f", lat)
        self.longitude = String(format:"%.6f", lon)
        
        LocationService.sharedInstance.stopUpdatingLocation()
    }
    func tracingLocationDidFailWithError(error: NSError) {
        print("tracing Location Error : \(error.description)")
        LocationService.sharedInstance.stopUpdatingLocation()
    }
}
