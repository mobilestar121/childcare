//
//  CCContactViewController.swift
//  ChildCare
//
//  Created by Mac on 24/02/17.
//  Copyright © 2017 Edward. All rights reserved.
//

import UIKit

class CCContactViewController: CCContentViewController {
    
    @IBOutlet weak var webView: UIWebView!

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        navigationItem.title = "Contact"
        
        let url : URL! = URL(string: Constants.ChildCareAPI.CONTACT)
        self.webView.loadRequest(URLRequest(url: url))
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
