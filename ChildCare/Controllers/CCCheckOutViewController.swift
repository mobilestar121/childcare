//
//  CCCheckOutViewController.swift
//  ChildCare
//
//  Created by Mac on 15/02/17.
//  Copyright © 2017 Edward. All rights reserved.
//

import UIKit
import SVProgressHUD
import CoreLocation

class CCCheckOutViewController: UIViewController {
    @IBOutlet weak var tableView: UITableView!
    
    var checkoutsArray = [CCCheckOut]()
    
    var lattitude: String?
    var longitude: String?
    var selectedParamDic:[String:String] = [:]

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        navigationItem.title = "Check-Out"
        
        addLeftNavBar()
        getCheckoutList()
        
        LocationService.sharedInstance.delegate = self
        self.lattitude = ""
        self.longitude = ""
        getCurrentLocation()
    }
    
    func addLeftNavBar() {
        let letfButton = UIButton()
        letfButton.setImage(UIImage(named: "back_icon"), for: .normal)
        letfButton.frame = CGRect(x:-40, y:0, width:30, height:30)
        letfButton.imageEdgeInsets = UIEdgeInsetsMake(0, -20, 0, 0)
        letfButton.addTarget(self, action: #selector(back), for: .touchUpInside)
        self.navigationItem.setLeftBarButton(UIBarButtonItem(customView: letfButton), animated: true);
        
        let rightButton = UIButton()
        rightButton.setImage(UIImage(named: "contact-icon"), for: .normal)
        rightButton.frame = CGRect(x:0, y:0, width:72, height:30)
        rightButton.imageEdgeInsets = UIEdgeInsetsMake(3, 0, 0, -10)
        rightButton.addTarget(self, action: #selector(openContact), for: .touchUpInside)
        self.navigationItem.setRightBarButton(UIBarButtonItem(customView: rightButton), animated: true);
    }
    func openContact() {
        let mobileNumber = CCUserInfoManager.sharedInstance.getCurrentUser()?.mobile
        self.callNumber(phoneNumber: mobileNumber!)
    }
    private func callNumber(phoneNumber:String) {
        if let phoneCallURL = URL(string: "tel://\(phoneNumber)") {
            let application:UIApplication = UIApplication.shared
            if (application.canOpenURL(phoneCallURL)) {
                application.open(phoneCallURL, options: [:], completionHandler: nil)
            }
        }
    }
    func back() {
        _ = self.navigationController?.popViewController(animated: true)
    }
    
    func showAlert(title:String, message:String) {
        let alert = UIAlertController(title: title, message: message, preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "Ok", style: .default, handler: nil))
        self.present(alert, animated: true, completion: nil)
    }
    
    
    func getCurrentLocation() {
        if CLLocationManager.locationServicesEnabled() {
            switch(CLLocationManager.authorizationStatus()) {
            case .notDetermined, .restricted, .denied:
                let title = "Turn On Location Services"
                let message = "Denied access Your Location:"
                let alert = UIAlertController(title: title, message: message, preferredStyle: .alert)
                alert.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler: nil))
                alert.addAction(UIAlertAction(title: "OK", style: .default) { (action) in
                    if let url = URL(string: UIApplicationOpenSettingsURLString) {
                        UIApplication.shared.open(url, options: [:], completionHandler: nil)
                    }
                })
                
                self.present(alert, animated: true, completion: nil)
                
                break
            case .authorizedAlways, .authorizedWhenInUse:
                print("Access")
                LocationService.sharedInstance.startUpdatingLocation()
                
                break
            }
        } else {
            print("Location services are not enabled")
            self.showAlert(title:"Error!", message: "Location services are not enabled")
        }
        
    }
    
    func getCheckout(checkout: NSDictionary) -> CCCheckOut {
        let tmpCheckout = CCCheckOut()
        
        if let id = checkout["id"] as? String {
            tmpCheckout.id = id
        }
        if let activity_name = checkout["activity_name"] as? String {
            tmpCheckout.activity_name = activity_name
        }
        if let child_details = checkout["child_details"] as? String {
            tmpCheckout.child_details = child_details
        }
        if let parent_details = checkout["parent_details"] as? String {
            tmpCheckout.parent_details = parent_details
        }
        if let check_out = checkout["check_out"] as? Bool {
            tmpCheckout.check_out = check_out
        }
        if let activity_checkin = checkout["activity_checkin"] as? Bool {
            tmpCheckout.activity_checkin = activity_checkin
        }
        if let activity_checkout = checkout["activity_checkout"] as? Bool {
            tmpCheckout.activity_checkout = activity_checkout
        }
        return tmpCheckout
    }
    
    func getCheckoutList() {
        let educatorId = CCUserInfoManager.sharedInstance.getCurrentUser()?.id
        
        SVProgressHUD.show()
        ChildCareService.sharedInstance.getCheckoutList(educatorId: educatorId!, onSuccess: { (response) in
            SVProgressHUD.dismiss()
            if let result = response.result.value as? NSDictionary{
                if let status = result["status"] as? Bool {
                    if status == true {
                        if let checkouts = result["result"] as? [NSDictionary] {
                            var tmpCheckoutsArray = [CCCheckOut]()
                            for checkout in checkouts {
                                tmpCheckoutsArray.append(self.getCheckout(checkout: checkout))
                            }
                            let filteredArray = tmpCheckoutsArray.filter() {
                                if let check_out = ($0 as CCCheckOut).check_out as Bool! {
                                    if check_out == true { //
                                        return true
                                    } else {
                                        return false
                                    }
                                } else {
                                    return false
                                }
                            }
                            self.checkoutsArray.removeAll(keepingCapacity: false)
                            self.checkoutsArray = filteredArray
                            
                            self.tableView.reloadData()
                        }
                    } else {
                        if let err = result["error"] as? NSDictionary {
                            if let message = err["message"] as? String {
                                self.showAlert(title:"Error!", message: message)
                                return
                            }
                        }
                    }
                }
            }
            }, onFailure: { (error) in
                SVProgressHUD.dismiss()
                self.showAlert(title:"Error!", message: error.localizedDescription)
        })
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func checkoutButtonClicked(sender: UIButton){
        let alertController = UIAlertController(title: "Child Care", message: "Are you sure you want to check out of this activity?", preferredStyle: .alert)
        
        let cancelAction = UIAlertAction(title: "Cancel", style: .cancel) { action in
            // ...
        }
        alertController.addAction(cancelAction)
        
        let OKAction = UIAlertAction(title: "OK", style: .default) { action in
            let index = sender.tag
            
            let activityId = self.checkoutsArray[index].id
            
            let isSignature = self.checkoutsArray[index].activity_checkout
            
            if self.lattitude == nil && self.longitude == nil {
                self.showAlert(title:"Error!", message: "Could not get location")
                return
            }
            let educatorId = CCUserInfoManager.sharedInstance.getCurrentUser()?.id
            
            if isSignature! {
                let paramDic = ["educatorId": educatorId!,
                                "activityId": activityId!,
                                "lat":self.lattitude!,
                                "lon":self.longitude!,
                                "index": "\(index)"]
                self.selectedParamDic = paramDic
                self.performSegue(withIdentifier: "showCheckOutSigSegue", sender: self)
            } else {
                
                ChildCareService.sharedInstance.checkOutDetail(educatorId: educatorId!, activityId: activityId!, lat: self.lattitude!, lon: self.longitude!, signature: "", onSuccess: { (response) in
                    if let result = response.result.value as? NSDictionary{
                        if let status = result["status"] as? Bool {
                            if status == true {
                                self.showAlert(title: "ChildCare", message: "Checked out succesfully")
                                self.checkoutsArray[index].check_out = false
                                self.tableView.reloadData()
                            } else {
                                if let err = result["error"] as? NSDictionary {
                                    if let message = err["message"] as? String {
                                        self.showAlert(title:"Error!", message: message)
                                        return
                                    }
                                }
                            }
                        }
                    }
                    }, onFailure: { (error) in
                        self.showAlert(title:"Error!", message: error.localizedDescription)
                })
            }
            
        }
        alertController.addAction(OKAction)
        
        self.present(alertController, animated: true) {
            // ...
        }
        
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "showCheckOutSigSegue" {
            let vc = segue.destination as! CCCheckOutSignatureViewController
            vc.paramDict = selectedParamDic
        }
    }

}

extension CCCheckOutViewController: LocationServiceDelegate {
    func tracingLocation(currentLocation: CLLocation) {
        let lat = currentLocation.coordinate.latitude
        let lon = currentLocation.coordinate.longitude
        self.lattitude = String(format:"%.6f", lat)
        self.longitude = String(format:"%.6f", lon)
        
        LocationService.sharedInstance.stopUpdatingLocation()
    }
    func tracingLocationDidFailWithError(error: NSError) {
        print("tracing Location Error : \(error.description)")
        LocationService.sharedInstance.stopUpdatingLocation()
    }
}

extension CCCheckOutViewController: UITableViewDataSource, UITableViewDelegate {
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1;
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.checkoutsArray.count
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        var cell = UITableViewCell()
        let _cell = tableView.dequeueReusableCell(withIdentifier: "CCCheckOutCell", for: indexPath) as! CCCheckOutCell
        let checkout = self.checkoutsArray[indexPath.row]
        if checkout.activity_name == nil {
            _cell.lblActivityName.text = ""
        } else {
            _cell.lblActivityName.text = checkout.activity_name
        }
        
        if checkout.child_details == nil {
            _cell.lblChildrenName.text = ""
        } else {
            _cell.lblChildrenName.text = checkout.child_details
        }
        
        if !checkout.check_out! {
            _cell.btnCheckout.isHidden = true
        } else {
            _cell.btnCheckout.tag = indexPath.row
            _cell.btnCheckout.addTarget(self, action: #selector(CCCheckOutViewController.checkoutButtonClicked), for: .touchUpInside)
        }
        
        cell = _cell
        return cell
    }
}
