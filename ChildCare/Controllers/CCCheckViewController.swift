//
//  CCCheckViewController.swift
//  ChildCare
//
//  Created by Mac on 06/02/17.
//  Copyright © 2017 Edward. All rights reserved.
//

import UIKit
import DropDown
import CoreLocation
import BEMCheckBox

class CCCheckViewController: UIViewController {

    @IBOutlet weak var tableView: UITableView!
    
    var activitiesArray = [CCActivity]()
    var parentsArray = [CCParent]()
    var childrenArray = [CCChild]()
    
    var selectedChildIdArray = [String]()
    
    var isActivityLoaded = false
    var isParentLoaded = false
    
    let chooseActivitiesDropDown = DropDown()
    let chooseParentsDropDown = DropDown()
    
    var selectedActivity: CCActivity?
    var selectedParent: CCParent?
    
    var lattitude: String?
    var longitude: String?
    var selectedParamDic:[String:String] = [:]
    
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        navigationItem.title = "Check-In"
        
        LocationService.sharedInstance.delegate = self
        
        addLeftNavBar()
        
        self.lattitude = ""
        self.longitude = ""
        getCurrentLocation()
        
    }
    func addLeftNavBar() {
        let letfButton = UIButton()
        letfButton.setImage(UIImage(named: "back_icon"), for: .normal)
        letfButton.frame = CGRect(x:-40, y:0, width:30, height:30)
        letfButton.imageEdgeInsets = UIEdgeInsetsMake(0, -20, 0, 0)
        letfButton.addTarget(self, action: #selector(back), for: .touchUpInside)
        self.navigationItem.setLeftBarButton(UIBarButtonItem(customView: letfButton), animated: true);
        
        let rightButton = UIButton()
        rightButton.setImage(UIImage(named: "contact-icon"), for: .normal)
        rightButton.frame = CGRect(x:0, y:0, width:72, height:30)
        rightButton.imageEdgeInsets = UIEdgeInsetsMake(3, 0, 0, -10)
        rightButton.addTarget(self, action: #selector(openContact), for: .touchUpInside)
        self.navigationItem.setRightBarButton(UIBarButtonItem(customView: rightButton), animated: true);
    }
    func openContact() {
        let mobileNumber = CCUserInfoManager.sharedInstance.getCurrentUser()?.mobile
        self.callNumber(phoneNumber: mobileNumber!)
    }
    private func callNumber(phoneNumber:String) {
        if let phoneCallURL = URL(string: "tel://\(phoneNumber)") {
            let application:UIApplication = UIApplication.shared
            if (application.canOpenURL(phoneCallURL)) {
                application.open(phoneCallURL, options: [:], completionHandler: nil)
            }
        }
    }
    func back() {
        _ = self.navigationController?.popViewController(animated: true)
    }
    
    func showAlert(title:String, message:String) {
        let alert = UIAlertController(title: title, message: message, preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "Ok", style: .default, handler: nil))
        self.present(alert, animated: true, completion: nil)
    }
    
    func getActivity(activity: NSDictionary) -> CCActivity {
        let tmpActivity = CCActivity()
        
        if let id = activity["id"] as? String {
            tmpActivity.id = id
        }
        if let activity_name = activity["activity_name"] as? String {
            tmpActivity.activity_name = activity_name
        }
        if let activity_desc = activity["activity_desc"] as? String {
            tmpActivity.activity_desc = activity_desc
        }
        if let activity_checkin = activity["activity_checkin"] as? Bool {
            tmpActivity.activity_checkin = activity_checkin
        }
        if let activity_checkout = activity["activity_checkout"] as? Bool {
            tmpActivity.activity_checkout = activity_checkout
        }
        return tmpActivity
    }
    
    func getActivities(onSuccess:@escaping (_ success: Bool) -> Void) {
        ChildCareService.sharedInstance.getActivity(onSuccess: { (response) in
            if let result = response.result.value as? NSDictionary{
                if let status = result["status"] as? Bool {
                    if status == true {
                        if let activities = result["result"] as? [NSDictionary] {
                            var tmpActivitiesArray = [CCActivity]()
                            for activity in activities {
                                tmpActivitiesArray.append(self.getActivity(activity: activity))
                            }
                            self.activitiesArray.removeAll(keepingCapacity: false)
                            self.activitiesArray = tmpActivitiesArray
                            
                            self.isActivityLoaded = true
                            
                            onSuccess(true)
                        }
                    } else {
                        onSuccess(false)
                        if let message = result["message"] as? String {
                            self.showAlert(title:"Error!", message: message)
                            return
                        }
                    }
                }
            }
            }, onFailure: { (error) in
                self.showAlert(title:"Error!", message: error.localizedDescription)
        })
    }
    
    func chooseActivity() {
        chooseActivitiesDropDown.show()
    }
    
    func setupActivityDropDown(label:UILabel) {
        chooseActivitiesDropDown.anchorView = label
        
        // Will set a custom with instead of anchor view width
        //		dropDown.width = 100
        
        // By default, the dropdown will have its origin on the top left corner of its anchor view
        // So it will come over the anchor view and hide it completely
        // If you want to have the dropdown underneath your anchor view, you can do this:
        chooseActivitiesDropDown.bottomOffset = CGPoint(x: 0, y: label.bounds.height)
        
        // You can also use localizationKeysDataSource instead. Check the docs.
        var tmpActivitiesTitelDataSource = [String] ()
        
        for activity in self.activitiesArray {
            tmpActivitiesTitelDataSource.append(activity.activity_name!)
        }
        chooseActivitiesDropDown.dataSource = tmpActivitiesTitelDataSource
        
        // Action triggered on selection
        chooseActivitiesDropDown.selectionAction = { (index, item) in
            label.text = item
            self.selectedActivity = self.activitiesArray[index]
        }
    }
    
    func getParent(parent: NSDictionary) -> CCParent {
        let tmpParent = CCParent()
        
        if let id = parent["id"] as? String {
            tmpParent.id = id
        }
        if let parent_name = parent["parent_name"] as? String {
            tmpParent.parent_name = parent_name
        }
        if let parent_lname = parent["parent_lname"] as? String {
            tmpParent.parent_lname = parent_lname
        }
        if let mobile = parent["mobile"] as? String {
            tmpParent.mobile = mobile
        }
        if let email = parent["email"] as? String {
            tmpParent.email = email
        }
        if let password_c = parent["password_c"] as? String {
            tmpParent.password_c = password_c
        }
        if let status = parent["status"] as? String {
            tmpParent.status = status
        }
        if let parent_agreement = parent["parent_agreement"] as? String {
            tmpParent.parent_agreement = parent_agreement
        }
        if let children = parent["children"] as? String {
            tmpParent.status = children
        }
        if let date_on = parent["date_on"] as? String {
            tmpParent.date_on = date_on
        }
        return tmpParent
    }
    
    func getParents(onSuccess:@escaping (_ success: Bool) -> Void) {
        let educatorId = CCUserInfoManager.sharedInstance.getCurrentUser()?.id
        ChildCareService.sharedInstance.getParent(educatorId:educatorId!, onSuccess: { (response) in
            if let result = response.result.value as? NSDictionary{
                if let status = result["status"] as? Bool {
                    if status == true {
                        if let parents = result["result"] as? [NSDictionary] {
                            var tmpParentsArray = [CCParent]()
                            for parent in parents {
                                tmpParentsArray.append(self.getParent(parent: parent))
                            }
                            self.parentsArray.removeAll(keepingCapacity: false)
                            self.parentsArray = tmpParentsArray
                            
                            self.isParentLoaded = true
                            
                            onSuccess(true)
                        }
                    } else {
                        onSuccess(false)
                        if let message = result["message"] as? String {
                            self.showAlert(title:"Error!", message: message)
                            return
                        }
                    }
                }
            }
            }, onFailure: { (error) in
                self.showAlert(title:"Error!", message: error.localizedDescription)
        })
    }
    
    func chooseParent() {
        chooseParentsDropDown.show()
    }
    
    func setupParentDropDown(label:UILabel) {
        chooseParentsDropDown.anchorView = label
        
        // Will set a custom with instead of anchor view width
        //		dropDown.width = 100
        
        // By default, the dropdown will have its origin on the top left corner of its anchor view
        // So it will come over the anchor view and hide it completely
        // If you want to have the dropdown underneath your anchor view, you can do this:
        chooseParentsDropDown.bottomOffset = CGPoint(x: 0, y: label.bounds.height)
        
        // You can also use localizationKeysDataSource instead. Check the docs.
        var tmpParentsTitelDataSource = [String] ()
        
        for parent in self.parentsArray {
            tmpParentsTitelDataSource.append(parent.parent_name!)
        }
        chooseParentsDropDown.dataSource = tmpParentsTitelDataSource
        
        // Action triggered on selection
        chooseParentsDropDown.selectionAction = { (index, item) in
            label.text = item
            self.selectedParent = self.parentsArray[index]
            self.getChildren(childId: (self.selectedParent?.id)!)
        }
    }
    
    // Get Children
    func getChild(child: NSDictionary) -> CCChild {
        let tmpChild = CCChild()
        
        if let id = child["id"] as? String {
            tmpChild.id = id
        }
        if let children_name = child["children_name"] as? String {
            tmpChild.children_name = children_name
        }
        return tmpChild
    }
    
    func getChildren(childId:String) {
        ChildCareService.sharedInstance.getChild(childId:childId, onSuccess: { (response) in
            if let result = response.result.value as? NSDictionary{
                if let status = result["status"] as? Bool {
                    if status == true {
                        if let children = result["result"] as? [NSDictionary] {
                            var tmpChildrenArray = [CCChild]()
                            for child in children {
                                tmpChildrenArray.append(self.getChild(child: child))
                            }
                            self.childrenArray.removeAll(keepingCapacity: false)
                            self.childrenArray = tmpChildrenArray
                            
//                            self.tableView.beginUpdates()
                            self.tableView!.reloadSections(NSIndexSet(index: 2) as IndexSet, with: .none)
//                            self.tableView.endUpdates()
                        }
                    } else {
                        if let err = result["error"] as? NSDictionary {
                            if let message = err["message"] as? String {
                                self.showAlert(title:"Error!", message: message)
                                return
                            }
                        }
                    }
                }
            }
            }, onFailure: { (error) in
                self.showAlert(title:"Error!", message: error.localizedDescription)
        })
    }
    
    // Check In
    func checkIn() {
        if selectedActivity == nil {
            self.showAlert(title:"Error!", message: "Please select activity")
            return
        }
        if selectedParent == nil {
            self.showAlert(title:"Error!", message: "Please select parent")
            return
        }
        if selectedChildIdArray.count == 0 {
            self.showAlert(title:"Error!", message: "Please select children")
            return
        }
        
        let educatorId = CCUserInfoManager.sharedInstance.getCurrentUser()?.id
        let childId = selectedChildIdArray.joined(separator: ",")
        
        if (selectedActivity?.activity_checkin)! {
            let paramDic = ["educatorId": educatorId!,
                            "parentId": (selectedParent?.id)!,
                            "childId":childId,
                            "activityId":(selectedActivity?.id)!,
                            "lat":self.lattitude!,
                            "lon":self.longitude!]
            self.selectedParamDic = paramDic 
            self.performSegue(withIdentifier: "showCheckInSigSegue", sender: self)
        } else {
            ChildCareService.sharedInstance.checkIn(educatorId: educatorId!, parentId: (selectedParent?.id)!, childId: childId, activityId: (selectedActivity?.id)!, lat: self.lattitude!, lon: self.longitude!, signature:"", onSuccess: { (response) in
                if let result = response.result.value as? NSDictionary{
                    if let status = result["status"] as? Bool {
                        if status == true {
                            self.showAlert(title: "Check-In success", message: "succesfully created")
                        } else {
                            if let err = result["error"] as? NSDictionary {
                                if let message = err["message"] as? String {
                                    self.showAlert(title:"Error!", message: message)
                                    return
                                }
                            }
                        }
                    }
                }
                }, onFailure: { (error) in
                    self.showAlert(title:"Error!", message: error.localizedDescription)
            })
        }
        
    }
    func getCurrentLocation() {
        if CLLocationManager.locationServicesEnabled() {
            switch(CLLocationManager.authorizationStatus()) {
            case .notDetermined, .restricted, .denied:
                let title = "Turn On Location Services"
                let message = "Denied access Your Location:"
                let alert = UIAlertController(title: title, message: message, preferredStyle: .alert)
                alert.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler: nil))
                alert.addAction(UIAlertAction(title: "OK", style: .default) { (action) in
                    if let url = URL(string: UIApplicationOpenSettingsURLString) {
                        UIApplication.shared.open(url, options: [:], completionHandler: nil)
                    }
                })
                
                self.present(alert, animated: true, completion: nil)
                
                break
            case .authorizedAlways, .authorizedWhenInUse:
                print("Access")
                LocationService.sharedInstance.startUpdatingLocation()
                
                break
            }
        } else {
            print("Location services are not enabled")
            self.showAlert(title:"Error!", message: "Location services are not enabled")
        }
        
    }
    func bemCheckBoxClicked(_ sender: BEMCheckBox) {
        
        
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "showCheckInSigSegue" {
            let vc = segue.destination as! CCCheckInSignatureViewController
            vc.paramDict = selectedParamDic
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

}
extension CCCheckViewController: CCChildCellDelegate {
    func checkBoxClicked(_ checkBox: BEMCheckBox) {
        let index = checkBox.tag
        if checkBox.on {
            let child = self.childrenArray[index]
            let childId = child.id
            selectedChildIdArray.append(childId!)
        } else {
            var i: Int?
            for x in 0 ..< selectedChildIdArray.count {
                let childId = selectedChildIdArray[x]
                if childId == childrenArray[index].id {
                    i = x
                    break
                }
            }
            selectedChildIdArray.remove(at: i!)
        }
    }
}
extension CCCheckViewController: LocationServiceDelegate {
    func tracingLocation(currentLocation: CLLocation) {
        let lat = currentLocation.coordinate.latitude
        let lon = currentLocation.coordinate.longitude
        self.lattitude = String(format:"%.6f", lat)
        self.longitude = String(format:"%.6f", lon)
        
        LocationService.sharedInstance.stopUpdatingLocation()
        
    }
    func tracingLocationDidFailWithError(error: NSError) {
        print("tracing Location Error : \(error.description)")
        LocationService.sharedInstance.stopUpdatingLocation()
    }
}

extension CCCheckViewController: UITableViewDelegate, UITableViewDataSource {
    func numberOfSections(in tableView: UITableView) -> Int {
        return 4
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if section == 0 {
            return 0
        } else if section == 1 {
            return 2
        } else if section == 2 {
            return childrenArray.count
        } else if section == 3 {
            return 1
        }
        return 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        var cell = UITableViewCell()
        if indexPath.section == 0 {
            let _cell = tableView.dequeueReusableCell(withIdentifier: "CCActivityTableViewCell", for: indexPath) as! CCActivityTableViewCell
            cell = _cell
        } else if indexPath.section == 1 {
            let _cell = tableView.dequeueReusableCell(withIdentifier: "CCSelectTypeCell", for: indexPath) as! CCSelectTypeCell
            _cell.showIndicator(isHidden: true)
            
            if indexPath.row == 0 {
                _cell.lblSelect.text = "Select Activity"
            } else if indexPath.row == 1 {
                _cell.lblSelect.text = "Select Parent"
            }
            
            cell = _cell
        } else if indexPath.section == 2 {
            let _cell = tableView.dequeueReusableCell(withIdentifier: "CCChildCell", for: indexPath) as! CCChildCell
            let child = self.childrenArray[indexPath.row]
            _cell.lblChildName.text = child.children_name!
            _cell.checkBox.tag = indexPath.row
            _cell.deleage = self
            
            cell = _cell
            
        } else if indexPath.section == 3 {
            let _cell = tableView.dequeueReusableCell(withIdentifier: "CCCheckInCell", for: indexPath) as! CCCheckInCell
            
            cell = _cell
        }
        
        return cell
    }
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        if section == 2 {
            if self.childrenArray.count > 0 {
                return 44.0
            }
            return 0.01
        }
        return 0.01
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if indexPath.section == 1 || indexPath.section == 2 || indexPath.section == 3 {
            return 56.0
        }
        return 0.01
    }
    
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let cell = UIView()
        if section == 2 {
            let _cell = self.tableView.dequeueReusableCell(withIdentifier: "CCCheckInSectionHeader") as! CCCheckInSectionHeader
            _cell.titleLabel.text = "Select Children"
            _cell.titleLabel.textAlignment = .left
            return _cell.contentView
        }
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        self.tableView.deselectRow(at: indexPath, animated: true)
        if indexPath.section == 0 {
            
        } else if indexPath.section == 1 {
            if indexPath.row == 0 {  // if activity
                if !isActivityLoaded {
                    let selectedCell:CCSelectTypeCell = tableView.cellForRow(at: indexPath) as! CCSelectTypeCell
                    selectedCell.showIndicator(isHidden: false)
                    
                    selectedCell.loadingIndicator.startAnimating()
                    self.getActivities(onSuccess: { (isSuccess) in
                        if isSuccess {
                            selectedCell.loadingIndicator.stopAnimating()
                            selectedCell.showIndicator(isHidden: true)
                            self.setupActivityDropDown(label:selectedCell.lblSelect)
                            
                            self.chooseActivity()
                        }
                    })
                } else {
                    self.chooseActivity()
                }
            } else { // case of parent
                if !isParentLoaded {
                    let selectedCell:CCSelectTypeCell = tableView.cellForRow(at: indexPath) as! CCSelectTypeCell
                    selectedCell.showIndicator(isHidden: false)
                    
                    selectedCell.loadingIndicator.startAnimating()
                    self.getParents(onSuccess: { (isSuccess) in
                        if isSuccess {
                            selectedCell.loadingIndicator.stopAnimating()
                            selectedCell.showIndicator(isHidden: true)
                            self.setupParentDropDown(label:selectedCell.lblSelect)
                            
                            self.chooseParent()
                        }
                    })
                } else {
                    self.chooseParent()
                }
            }
        }
        
        else if indexPath.section == 3 {
            if indexPath.row == 0 {
                self.checkIn()
            }
        }
    }
}
