//
//  CCNewsViewController.swift
//  ChildCare
//
//  Created by Mac on 15/02/17.
//  Copyright © 2017 Edward. All rights reserved.
//

import UIKit
import SVProgressHUD
import AlamofireImage

class CCNewsViewController: UIViewController {
    
    @IBOutlet weak var tableView: UITableView!
    var newsArray = [CCNews]()
    var selectedNews: CCNews?
    

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        navigationItem.title = "News"
        self.tableView.estimatedRowHeight = 140
        
        addLeftNavBar()
        getNews()
    }
    
    func addLeftNavBar() {
        let letfButton = UIButton()
        letfButton.setImage(UIImage(named: "back_icon"), for: .normal)
        letfButton.frame = CGRect(x:-40, y:0, width:30, height:30)
        letfButton.imageEdgeInsets = UIEdgeInsetsMake(0, -20, 0, 0)
        letfButton.addTarget(self, action: #selector(back), for: .touchUpInside)
        self.navigationItem.setLeftBarButton(UIBarButtonItem(customView: letfButton), animated: true);
        
        let rightButton = UIButton()
        rightButton.setImage(UIImage(named: "contact-icon"), for: .normal)
        rightButton.frame = CGRect(x:0, y:0, width:72, height:30)
        rightButton.imageEdgeInsets = UIEdgeInsetsMake(3, 0, 0, -10)
        rightButton.addTarget(self, action: #selector(openContact), for: .touchUpInside)
        self.navigationItem.setRightBarButton(UIBarButtonItem(customView: rightButton), animated: true);
    }
    func openContact() {
        let mobileNumber = CCUserInfoManager.sharedInstance.getCurrentUser()?.mobile
        self.callNumber(phoneNumber: mobileNumber!)
    }
    private func callNumber(phoneNumber:String) {
        if let phoneCallURL = URL(string: "tel://\(phoneNumber)") {
            let application:UIApplication = UIApplication.shared
            if (application.canOpenURL(phoneCallURL)) {
                application.open(phoneCallURL, options: [:], completionHandler: nil)
            }
        }
    }
    func back() {
        _ = self.navigationController?.popViewController(animated: true)
    }
    
    func showAlert(title:String, message:String) {
        let alert = UIAlertController(title: title, message: message, preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "Ok", style: .default, handler: nil))
        self.present(alert, animated: true, completion: nil)
    }
    
    func getNews(news: NSDictionary) -> CCNews {
        let tmpNews = CCNews()
        
        if let id = news["id"] as? String {
            tmpNews.id = id
        }
        if let news_name = news["news_name"] as? String {
            tmpNews.news_name = news_name
        }
        if let news_desc = news["news_desc"] as? String {
            tmpNews.news_desc = news_desc
        }
        if let news_type = news["news_type"] as? String {
            tmpNews.news_type = news_type
        }
        if let news_image = news["news_image"] as? String {
            tmpNews.news_image = news_image
        }
        if let status = news["status"] as? String {
            tmpNews.status = status
        }
        if let date_on = news["date_on"] as? String {
            tmpNews.date_on = date_on
        }
        return tmpNews
    }
    
    func getNews() {
        SVProgressHUD.show()
        ChildCareService.sharedInstance.getNews(onSuccess: { (response) in
            SVProgressHUD.dismiss()
            if let result = response.result.value as? NSDictionary{
                if let status = result["status"] as? Bool {
                    if status == true {
                        if let newsItems = result["result"] as? [NSDictionary] {
                            var tmpNewsArray = [CCNews]()
                            for news in newsItems {
                                tmpNewsArray.append(self.getNews(news: news))
                            }
                            self.newsArray.removeAll(keepingCapacity: false)
                            self.newsArray = tmpNewsArray
                            
                            self.tableView.reloadData()
                        }
                    } else {
                        if let message = result["message"] as? String {
                            self.showAlert(title:"Error!", message: message)
                            return
                        }
                    }
                }
            }
            }, onFailure: { (error) in
                SVProgressHUD.dismiss()
                self.showAlert(title:"Error!", message: error.localizedDescription)
        })
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

}

extension CCNewsViewController: UITableViewDataSource, UITableViewDelegate {
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return newsArray.count
    }
    
    func convertDateFormat(origin: String) -> String {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-mm-dd" //Your date format
        let date = dateFormatter.date(from: origin) //according to date format your date string
        
        dateFormatter.dateFormat = "dd MMMM, yyyy" //Your New Date format as per requirement change it own
        let newDate = dateFormatter.string(from: date!)
        
        return newDate
        
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        var cell = UITableViewCell()
        let _cell = tableView.dequeueReusableCell(withIdentifier: "CCNewsCell", for: indexPath) as! CCNewsCell
        
        let news = self.newsArray[indexPath.row]
        _cell.lblDate.text = self.convertDateFormat(origin: news.date_on!)
        _cell.lblDescription.text = news.news_name!
        
        if (news.news_image != nil && news.news_image != "") {
            _cell.configureCell(with: news.news_image!, placeholderImage: UIImage(named: "news_icon")!)
        }
        
        cell = _cell
        
        return cell
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 132.0
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        selectedNews = self.newsArray[indexPath.row]
        performSegue(withIdentifier: "showNewsDetailSegue", sender: self)
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if(segue.identifier == "showNewsDetailSegue") {
            let vc = segue.destination as! CCNewsDetailViewController
            vc.news = selectedNews
        }
    }
}
