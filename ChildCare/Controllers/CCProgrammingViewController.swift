//
//  CCProgrammingViewController.swift
//  ChildCare
//
//  Created by Mac on 21/02/17.
//  Copyright © 2017 Edward. All rights reserved.
//

import UIKit
import SVProgressHUD

class CCProgrammingViewController: UIViewController {
    
    @IBOutlet weak var tableView: UITableView!
    
    var programsArray = [CCProgram]()
    var selectedProgramm: CCProgram?

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        navigationItem.title = "Programming & Planning"
        
        addLeftNavBar()
        
        requestProgram()
    }
    
    func addLeftNavBar() {
        let letfButton = UIButton()
        letfButton.setImage(UIImage(named: "back_icon"), for: .normal)
        letfButton.frame = CGRect(x:-40, y:0, width:30, height:30)
        letfButton.imageEdgeInsets = UIEdgeInsetsMake(0, -20, 0, 0)
        letfButton.addTarget(self, action: #selector(back), for: .touchUpInside)
        self.navigationItem.setLeftBarButton(UIBarButtonItem(customView: letfButton), animated: true);
        
        let rightButton = UIButton()
        rightButton.setImage(UIImage(named: "contact-icon"), for: .normal)
        rightButton.frame = CGRect(x:0, y:0, width:72, height:30)
        rightButton.imageEdgeInsets = UIEdgeInsetsMake(3, 0, 0, -10)
        rightButton.addTarget(self, action: #selector(openContact), for: .touchUpInside)
        self.navigationItem.setRightBarButton(UIBarButtonItem(customView: rightButton), animated: true);
    }
    func openContact() {
        let mobileNumber = CCUserInfoManager.sharedInstance.getCurrentUser()?.mobile
        self.callNumber(phoneNumber: mobileNumber!)
    }
    private func callNumber(phoneNumber:String) {
        if let phoneCallURL = URL(string: "tel://\(phoneNumber)") {
            let application:UIApplication = UIApplication.shared
            if (application.canOpenURL(phoneCallURL)) {
                application.open(phoneCallURL, options: [:], completionHandler: nil)
            }
        }
    }
    func back() {
        _ = self.navigationController?.popViewController(animated: true)
    }
    
    func showAlert(message:String) {
        let alert = UIAlertController(title: "Error!", message: message, preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "Ok", style: .default, handler: nil))
        self.present(alert, animated: true, completion: nil)
    }
    
    func getProgram(program: NSDictionary) -> CCProgram {
        let tmpProgram = CCProgram()
        
        if let id = program["id"] as? String {
            tmpProgram.id = id
        }
        if let program_name = program["program_name"] as? String {
            tmpProgram.program_name = program_name
        }
        if let program_file = program["program_file"] as? String {
            tmpProgram.program_file = program_file
        }
        if let status = program["status"] as? String {
            tmpProgram.status = status
        }
        if let date_on = program["date_on"] as? String {
            tmpProgram.date_on = date_on
        }
        return tmpProgram
    }
    
    func requestProgram() {
        SVProgressHUD.show()
        ChildCareService.sharedInstance.getProgramm(onSuccess: { (response) in
            debugPrint(response)
            SVProgressHUD.dismiss()
            if let result = response.result.value as? NSDictionary{
                if let status = result["status"] as? Bool {
                    if status == true {
                        if let programItems = result["result"] as? [NSDictionary] {
                            var tmpProgramsArray = [CCProgram]()
                            for program in programItems {
                                tmpProgramsArray.append(self.getProgram(program: program))
                            }
                            self.programsArray.removeAll(keepingCapacity: false)
                            self.programsArray = tmpProgramsArray
                            
                            self.tableView.reloadData()
                        }
                    } else {
                        if let message = result["message"] as? String {
                            self.showAlert(message: message)
                            return
                        }
                    }
                }
            }
            }, onFailure: { (error) in
                debugPrint(error)
                SVProgressHUD.dismiss()
                self.showAlert(message: error.localizedDescription)
        })
    }


    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
}

extension CCProgrammingViewController: UITableViewDataSource, UITableViewDelegate {
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.programsArray.count
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        var cell = UITableViewCell()
        
        let _cell = tableView.dequeueReusableCell(withIdentifier: "CCProgramCell", for: indexPath) as! CCProgramCell
        let program = self.programsArray[indexPath.row]
        _cell.lblTitle.text = program.program_name!
        
        cell = _cell
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        selectedProgramm = self.programsArray[indexPath.row]
        performSegue(withIdentifier: "showProgramDetailSegue", sender: self)
    }

    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if(segue.identifier == "showProgramDetailSegue") {
            let vc = segue.destination as! CCDetailProgramViewController
            vc.programm = selectedProgramm
        }
    }
}
